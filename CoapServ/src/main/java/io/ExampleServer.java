package io;

import java.util.concurrent.Executors;

import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.CoapServer;

import services.HelloWorld;

/**
 * This is an example server that contains a few resources for demonstration.
 * 
 * @author Martin Lanter
 * @doc https://github.com/mkovatsc/Californium
 * @doc https://github.com/eclipse/californium
 */
public class ExampleServer {
	
	public static void main(String[] args) throws Exception {
		CoapServer server = new CoapServer();
		server.setExecutor(Executors.newScheduledThreadPool(4));
		
		server.add(new HelloWorld("hello"));
		
		server.start();
	}
	
	/*
	 *  Sends a GET request to itself
	 */
	public static void selfTest() {
		try {
			Request request = Request.newGet();
			request.setURI("localhost:5683/hello");
			request.send();
			Response response = request.waitForResponse(1000);
			System.out.println("received "+response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}