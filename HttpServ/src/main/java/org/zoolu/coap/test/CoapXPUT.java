package org.zoolu.coap.test;



import org.zoolu.coap.blockwise.*;
import org.zoolu.coap.core.*;
import org.zoolu.coap.message.*;
import org.zoolu.net.SocketAddress;

import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;



/** Very simple CoAP client for PUTting a resource value. 
 * <p/>
 * It supports blockwise transfer (draft-ietf-core-block) and resource observation (draft-ietf-core-observe) CoAP extensions.
 */
public class CoapXPUT implements BlockwiseTransactionClientListener
{
   /** CoAP provider */
   CoapProvider coap_provider;


   /** Puts a resource.
    * @param resource_name the name of the resource
    * @param resource_value the value of the resource */
   public CoapXPUT(String resource_name, String resource_value) throws SocketException, URISyntaxException
   {  coap_provider=new CoapProvider(CoapProvider.ANY_PORT);
      URI resource_uri=new URI(resource_name);
      SocketAddress server_soaddr=new SocketAddress(resource_uri.getHost(),resource_uri.getPort());
      CoapRequest req=(CoapRequest)CoapMessageFactory.createCONRequest(CoapMethod.PUT,resource_uri).setPayload(resource_value.getBytes());
      new BlockwiseTransactionClient(coap_provider,server_soaddr,this).request(req);
   }


   /** When a new CoAP response message is received.
    * @param block1_client the blockwise transfer client
    * @param msg the received CoAP response */
   @Override
   public void onTransactionResponse(BlockwiseTransactionClient btc, CoapMessage resp)
   {  System.out.println(CoapResponseCode.getDescription(resp.getCode()));
      byte[] payload=resp.getPayload();
      if (payload!=null) System.out.println(new String(payload));
      coap_provider.halt();
   }


   /** When a RST is received for a Confirmable request or transaction timeout expired.
    * @param btc the blockwise transaction client */
   @Override
   public void onTransactionFailure(BlockwiseTransactionClient btc)
   {  System.out.println("Failure..");
      coap_provider.halt();
   }

   
   /** The main method.
    * @param args command-line arguments */
   public static void main(String[] args)
   {  try
      {  new CoapXPUT(args[0],args[1]);
      }
      catch (Exception e)
      {  e.printStackTrace();
      }
   }

   
}
