package org.zoolu.coap.test;



import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.coap.blockwise.Block2Server;
import org.zoolu.coap.core.*;
import org.zoolu.coap.message.*;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.net.UdpSocket;
import org.zoolu.util.ByteUtils;
import org.zoolu.util.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;



/** Very simple CoAP server.
 * It maintain a DB of local resources and responds to CoAP requests.
 */
public class CoapTestServer
{
   /** Root resource */
   private static String ROOT_RESOURCE=
      "This is a test server based on mjCoAP library. \n"+
      "\n"+
      "The mjCoAP is an open source lightweight Java-based \n"+
      "implementation of CoAP, fully RFC-compliant.\n"+
      "It aims at simplifying the development of CoAP-based \n"+
      "IoT applications. \n"+
      "For more information see: http://www.mjcoap.org";
   
   
   /** Default resource name */
   private static final String DEFAULT_RESOURCE_NAME="/test";

   /** Default resource value */
   private static final byte[] DEFAULT_RESOURCE_VALUE=("hello world").getBytes();
   
   /** Maximum payload size for blockwise transfer (value 0 means no blockwise transfer) */
   private static int MAX_BLOCK_SIZE=0;
   
   /** Verbose level */
   private static int VERBOSE_LEVEL=0;

   /** Whether the server internally acts in stateful (true) or stateless (false) mode */
   private static boolean STATEFUL_MODE=false;

   /** Receiver buffer size (default is 1MB) */
   private static int RECEIVER_BUFFER_SIZE=1024*1024;

   /** Time waited before responding */
   private static long RESPONSE_DELAY=0;

   /** Prompt for exiting */
   private static boolean PROMPT=false;
   
   /** total number of received requests */
   private static long req_count=0;

   /** total number of sent responses */
   private static long resp_count=0;
   
   
   /** CoAP provider */
   CoapProvider coap_provider;
   
   /** Server resources */
   Hashtable<String,byte[]> resource_db=new Hashtable<String,byte[]>();



   /** Creates a new server. */
  public CoapTestServer() throws SocketException
  {  init(CoapProvider.DEFAUL_UDP_PORT);
  }

  
   /** Creates a new server.
     * @param local_port CoAP UDP port */
   public CoapTestServer(int local_port) throws SocketException
   {  init(local_port);
   }

   
   /** Creates a new server.
    * @param coap_provider CoAP provider */
   public CoapTestServer(CoapProvider coap_provider)
   {  this.coap_provider=coap_provider;
      init();
   }

  
   /** Creates a new server.
    * @param args command-line arguments */
   public CoapTestServer(String[] args) throws  SocketException, IOException
   {  int local_port=CoapProvider.DEFAUL_UDP_PORT;
      for (int i=0; i<args.length; i++)
      {  String option;
         if (args[i].equals("-h"))
         {  printHelp();
            System.exit(0);
         }
         // else
         if (args[i].startsWith(option="-p")) 
         {  String str=(args[i].length()==option.length())? args[++i] : args[i].substring(option.length());
            local_port=Integer.parseInt(str);
            continue;
         }
         // else
         if (args[i].startsWith(option="-a")) 
         {  String resource_name=(args[i].length()==option.length())? args[++i] : args[i].substring(option.length());
            String str=args[++i];
            byte[] resource_value=str.startsWith("0x")? ByteUtils.hexToBytes(str) : str.getBytes();
            addResource(resource_name,resource_value);
            continue;
         }
         // else
         if (args[i].equals(option="--prompt")) 
         {  PROMPT=true;
            continue;
         }
         // else
         if (args[i].startsWith(option="--debug")) 
         {  String str=(args[i].length()==option.length())? args[++i] : args[i].substring(option.length());
            VERBOSE_LEVEL=Integer.parseInt(str);
            if (VERBOSE_LEVEL>0) Logger.setDefaultLogger(new Logger(System.out,Logger.LEVEL_DEBUG));
            continue;
         }
          // else
         if (args[i].equals(option="--stateful")) 
         {  STATEFUL_MODE=true;
            continue;
         }
         // else
         if (args[i].startsWith(option="--delay")) 
         {  String str=(args[i].length()==option.length())? args[++i] : args[i].substring(option.length());
            RESPONSE_DELAY=Long.parseLong(str);
            continue;
         }
         // else
         if (args[i].startsWith(option="--recv-buff-size")) 
         {  String str=(args[i].length()==option.length())? args[++i] : args[i].substring(option.length());
            char c=str.charAt(str.length()-1);
            if (c=='M') RECEIVER_BUFFER_SIZE=1024*1024*Integer.parseInt(str.substring(0,str.length()-1));
            else
            if (c=='K' || c=='k') RECEIVER_BUFFER_SIZE=1024*Integer.parseInt(str.substring(0,str.length()-1));
            else
               RECEIVER_BUFFER_SIZE=Integer.parseInt(str);
            continue;
         }
         // else
         if (args[i].startsWith(option="--error")) 
         {  String str=(args[i].length()==option.length())? args[++i] : args[i].substring(option.length());
            CoapProvider.PACKET_ERROR_RATE=Double.parseDouble(str);
            continue;
         }
         // else
         if (args[i].equals(option="--skip")) 
         {  continue;
         }
         // else
         throw new IOException("CoapServer: Unknown argument '"+args[i]+"'");
      }
      init(local_port);
   }

  
   /** Initializes the server. 
    * @param local_port CoAP UDP port */
   private void init(int local_port) throws SocketException
   {  //coap_provider=new CoapProvider(local_port);
      UdpSocket udp_socket=new UdpSocket(local_port);
      udp_socket.setReceiverBufferSize(RECEIVER_BUFFER_SIZE);
      coap_provider=new CoapProvider(udp_socket);
      init();
   }


   /** Initializes the server. */
   private void init()
   {  if (coap_provider!=null)
      {  CoapProviderListener this_coap_provider_listener=new CoapProviderListener()
         {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
            {  processReceivedMessage(coap_provider,msg);
            }
         };
         coap_provider.addListener(CoapMethodId.ANY,this_coap_provider_listener);
      }
   }

 
   /** Adds a new resource to this server.
    * @param resource_name the resource name
    * @param resource_value the resource value */
   public synchronized void addResource(String resource_name, byte[] resource_value)
   {  resource_db.put(resource_name,resource_value);
   }

   
   /** Whether there is a given resource.
   * @param resource_name the resource name
   * @return <i>true</i> if the resource is present */
   public synchronized boolean hasResource(String resource_name)
   {  return resource_db.containsKey(resource_name);
   }

   
   /** Gets a given resource.
    * @param resource_name the resource name
    * @return the resource value */
   public synchronized byte[] getResource(String resource_name)
   {  if (resource_name.equals("/.well-known/core"))
      {  StringBuffer sb=new StringBuffer();
         sb.append("</.well-known/core>");
         for (Enumeration<String> i=resource_db.keys(); i.hasMoreElements(); )
         {  resource_name=i.nextElement();
            if (resource_name.length()>0) sb.append(',').append('<').append(resource_name).append('>');
         }
         return sb.toString().getBytes();
      }
      else return resource_db.get(resource_name);
   }

   
   /** Removes a given resource.
    * @param resource_name the resource name */
   public synchronized void removeResource(String resource_name)
   {  resource_db.remove(resource_name);
   }

   
   /** Gets all resource names.
   * @return a set of all resource names */
   public Set<String> getResources()
   {  return resource_db.keySet();
   }

   
   /** Processes an incoming CoAP message received by a CoAP provider.
    * @param coap_provider the CoAP provider
    * @param msg the received message */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  try
      {  if (VERBOSE_LEVEL>0) println("onReceivedMessage(): "+CoapProtocolAnalyzer.analyze(msg).toString(VERBOSE_LEVEL));
         req_count++;
      
         if (!msg.isRequest()) return;
         // else
         
         int response_code=CoapResponseCode.getCode(4,0);
         byte[] payload=null;
         
         // get resource name
         String resource_name=null;
         CoapOption[] options=msg.getOptions();
         if (options!=null)
         {  StringBuffer uri_path=new StringBuffer();
            for (int i=0; i<options.length; i++)
            {  CoapOption opt=options[i];
               if (opt.getOptionNumber()==CoapOptionNumber.UriPath) uri_path.append('/').append(opt.getValueAsString());
            }
            resource_name=uri_path.toString();
         }
         
         if (resource_name!=null)
         {  
            if (msg.getCode()==CoapMethod.GET.getCode())
            {  payload=getResource(resource_name);
               if (payload!=null) response_code=CoapResponseCode.getCode(2,5);
               else response_code=CoapResponseCode.getCode(4,4);
            }
            else
            if (msg.getCode()==CoapMethod.PUT.getCode())
            {  if (hasResource(resource_name))
               {  removeResource(resource_name);
                  response_code=CoapResponseCode.getCode(2,4);
               }
               else response_code=CoapResponseCode.getCode(2,1);
               
               byte[] resource_value=msg.getPayload();
               addResource(resource_name,resource_value);
            }
            else
            if (msg.getCode()==CoapMethod.DELETE.getCode())
            {  if (hasResource(resource_name))
               {  removeResource(resource_name);
                  response_code=CoapResponseCode.getCode(2,2);
               }
               else response_code=CoapResponseCode.getCode(4,4);
            }
            else
            {  response_code=CoapResponseCode.getCode(4,5);
               payload=null;
            }
         }
         //else response_code=CoapResponseCode.getCode(4,0);
         
         // send the response
         respond(msg,response_code,payload);
      }
      catch (Exception e)
      {  e.printStackTrace();
      }
   }
   

   /** Creates and sends a response.
    * @param request the request message
    * @param response_code the response code
    * @param payload the response payload */
   protected void respond(CoapMessage req, int response_code, byte[] payload)
   {  if (VERBOSE_LEVEL>0) println("respond(): "+CoapResponseCode.getDescription(response_code)+ " "+(payload!=null? ByteUtils.asHex(payload) : null));
      
      //if (STATEFUL_MODE) new CoapTransactionServer(coap_provider,req,null).respond(response_code,payload);
      //else coap_provider.send(BasicCoapMessageFactory.createPiggyBackedResponse(req,response_code,payload),req.getRemoteSoAddress());
      
      // create response
      CoapMessage resp=BasicCoapMessageFactory.createResponse(req,response_code,payload);
      resp=Block2Server.blockwiseResponse(req,resp,MAX_BLOCK_SIZE);
      
      // send message
      if (VERBOSE_LEVEL>0) println("respond(): "+CoapProtocolAnalyzer.analyze(resp).toString(VERBOSE_LEVEL));
      if (RESPONSE_DELAY<=0)
      {  // respond now
         if (STATEFUL_MODE) new CoapTransactionServer(coap_provider,req,null).respond(resp);
         else
         {   if (req.isCON())
             {  resp.setACK();
                resp.setMessageId(req.getMessageId());
             }
             coap_provider.send(resp,req.getRemoteSoAddress());
             resp_count++;
         }
      }
      else
      {  // respond later
         final CoapMessage delayed_resp=resp;
         final CoapTransactionServer ts=new CoapTransactionServer(coap_provider,req,null);
         new Thread()
         {  public void run()
            {  try {  Thread.sleep(RESPONSE_DELAY);  } catch (Exception e) {};
               ts.respond(delayed_resp);
            }
         }.start();
      }
   }

   
   /** Stops the server. */
   public void halt()
   {  coap_provider.halt();
   }


   //**************************** STATIC ****************************
   
   /** Prints a message onto the standard output.
     * @param str the message to be printed */
   private static void println(String str)
   {  System.out.println(str);
   }

   
   /** Reads a line from standard input.
    * @return the line */
   private static String readln()
   {  try
      {  return new BufferedReader(new InputStreamReader(System.in)).readLine();
      }
      catch (Exception e)
      {  e.printStackTrace();
         return null;
      }
   }


   /** Exits. */
   private static void exit()
   {  System.exit(0);
   }


   /** Prints out a help. */
   private static void printHelp()
   {  println("\nUsage:\n   java "+CoapTestServer.class.getName()+" [options]");     
      println("   options:");
      println("   -h                  prints this help");
      println("   -p <port>           local UDP port");
      println("   -a <name> <value>   add a resource; value can be ASCII or HEX (0x..)");
      println("   --prompt            command prompt for exiting");
      println("   --debug <level>     debug level");
   }


   /** The main method.
    * @param args the arguments passed to this class at command-line */
   public static void main(String[] args)
   {
      try
      {  CoapTestServer server=new CoapTestServer(args);
         if (server.getResources().size()==0) server.addResource(DEFAULT_RESOURCE_NAME,DEFAULT_RESOURCE_VALUE);
         server.addResource("",ROOT_RESOURCE.getBytes());
         println("mjCoAP server is running - "+CoapTestServer.class.getCanonicalName());
         if (PROMPT)
         {  println("Press ENTER to quit");
            readln();
            println("closing..");
            server.halt();
            println("received/processed requests: "+req_count);
         }  
      }
      catch (SocketException e)
      {  e.printStackTrace();
         exit();
      }
      catch (IOException e)
      {  e.printStackTrace();
         printHelp();
         exit();
      }        
   }
   
}
