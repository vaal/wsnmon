package org.zoolu.coap.test;



import org.zoolu.coap.core.*;
import org.zoolu.coap.message.*;
import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.net.SocketAddress;

import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;



/** Very simple CoAP client for GETting a resource. 
 */
public class CoapGET implements CoapTransactionClientListener
{
   /** Verbose level */
   static int VERBOSE_LEVEL=0;
   
   /** CoAP provider */
   CoapProvider coap_provider;


   /** Gets a remote resource.
    * @param resource_name the name of the resource */
   public CoapGET(String resource_name) throws SocketException, URISyntaxException
   {  coap_provider=new CoapProvider(CoapProvider.ANY_PORT);
      URI resource_uri=new URI(resource_name);
      SocketAddress server_soaddr=new SocketAddress(resource_uri.getHost(),resource_uri.getPort());
      CoapRequest req=CoapMessageFactory.createCONRequest(CoapMethod.GET,resource_uri);
      if (VERBOSE_LEVEL>0) System.out.println(CoapProtocolAnalyzer.analyze(req).toString(VERBOSE_LEVEL-1));
      new CoapTransactionClient(coap_provider,server_soaddr,this).request(req);
   }


   /** When a CoAP response is received for the pending request.
   * @param tc the transaction client
   * @param msg the received CoAP response */
   @Override
   public void onTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
   {  if (VERBOSE_LEVEL>0) System.out.println(CoapProtocolAnalyzer.analyze(resp).toString(VERBOSE_LEVEL-1));
      System.out.println(CoapResponseCode.getDescription(resp.getCode()));
      byte[] payload=resp.getPayload();
      if (payload!=null) System.out.println(new String(payload));
      coap_provider.halt();
   }

   
   /** When a RST is received for a Confirmable request or transaction timeout expired.
   * @param tc the transaction client */
   @Override
   public void onTransactionFailure(CoapTransactionClient tc)
   {  System.out.println("Failure..");
      coap_provider.halt();
   }

   
   /** The main method.
    * @param args command-line arguments */
   public static void main(String[] args)
   {  try
      {  new CoapGET(args[0]);
      }
      catch (Exception e)
      {  e.printStackTrace();
      }
   }

   
}
