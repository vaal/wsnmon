package org.zoolu.coap.test;



import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.coap.blockwise.*;
import org.zoolu.coap.core.*;
import org.zoolu.coap.message.*;
import org.zoolu.coap.observe.*;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.net.UdpSocket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.util.Vector;



/** Very simple CoAP server with only one resource.
 * <p/>
 * It supports blockwise transfer (draft-ietf-core-block) and resource observation (draft-ietf-core-observe) CoAP extensions.
 */
public class CoapXServer
{
   /** Logs debug message */
   private static void debug(String str)
   {  System.out.println("DEBUG: CoapTestServer: "+str);
   }

   /** Maximum payload size for blockwise transfer (value 0 means no blockwise transfer) */
   private static int MAX_PAYLOAD_SIZE=64;

   /** CoAP provider */
   CoapProvider coap_provider;
   
   /** Resource name */
   String resource_name;

   /** Resource value */
   String resource_value;
   
   /** This notifier listener */
   NotifierListener this_notifier_listener;
   
   /** Notifiers */
   Vector<Notifier> notifiers=new Vector<Notifier>();

   

   /** Creates a new server.
     * @param local_port CoAP UDP port */
   public CoapXServer(String resource_name, String resource_value) throws SocketException
   {  this.resource_name=resource_name;
      this.resource_value=resource_value;
      //coap_provider=new CoapProvider(local_port);
      UdpSocket udp_socket=new UdpSocket(CoapProvider.DEFAUL_UDP_PORT);
      coap_provider=new CoapProvider(udp_socket);
      CoapProviderListener this_coap_provider_listener=new CoapProviderListener()
      {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
         {  processReceivedMessage(coap_provider,msg);
         }
      };
      coap_provider.addListener(CoapMethodId.ANY,this_coap_provider_listener);
      
      this_notifier_listener=new NotifierListener()
      {  @Override
         public void onNotificationCancelled(Notifier notifier)
         {  processNotificationCancelled(notifier);
         }   
      };
   }

 
   /** Processes an incoming CoAP message received by a CoAP provider.
    * @param coap_provider the CoAP provider
    * @param msg the received message */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  debug("processReceivedMessage(CoapProvider,CoapMessage): "+CoapProtocolAnalyzer.analyze(msg).toString());
      BlockwiseTransactionServerListener this_bts_listener=new BlockwiseTransactionServerListener()
      {  @Override
         public void onReceivedRequest(BlockwiseTransactionServer bt_server, CoapMessage req)
         {  processReceivedRequest(bt_server,req);
         }
         @Override
         public void onTransactionFailure(CoapTransactionServer bts)
         {  // do nothing? 
         }
      };
      new BlockwiseTransactionServer(coap_provider,msg,MAX_PAYLOAD_SIZE,this_bts_listener);
   }
   
   
   /** Processes an incoming CoAP message received by a CoAP provider.
    * @param bts the blockwise transaction server
    * @param msg the received message */
   private void processReceivedRequest(BlockwiseTransactionServer bts, CoapMessage msg)
   {  debug("processReceivedRequest(Block1Server,CoapMessage): "+CoapProtocolAnalyzer.analyze(msg).toString());
      try
      {  if (!msg.isRequest()) return;
         // else
         
         int response_code=CoapResponseCode.getCode(4,0);
         byte[] payload=null;
         
         // get resource name
         String resource_name=null;
         CoapOption[] options=msg.getOptions();
         if (options!=null)
         {  StringBuffer uri_path=new StringBuffer();
            for (int i=0; i<options.length; i++)
            {  CoapOption opt=options[i];
               if (opt.getOptionNumber()==CoapOptionNumber.UriPath) uri_path.append('/').append(opt.getValueAsString());
            }
            resource_name=uri_path.toString();
         }
         
         if (this.resource_name.equals(resource_name))
         {  
            if (msg.getCode()==CoapMethod.GET.getCode())
            {  if (msg.hasOption(CoapOptionNumber.Observe)) 
               {  notifiers.addElement(new Notifier(coap_provider,msg,this_notifier_listener));
               }
               payload=this.resource_value.getBytes();
               response_code=CoapResponseCode.getCode(2,5);
            }
            else
            if (msg.getCode()==CoapMethod.PUT.getCode())
            {  this.resource_value=new String(msg.getPayload());
               for (int i=0; i<notifiers.size(); i++) notifiers.elementAt(i).notify(this.resource_value.getBytes());
               response_code=CoapResponseCode.getCode(2,4);
            }
            else
            {  response_code=CoapResponseCode.getCode(4,5);
            }
         }
         else response_code=CoapResponseCode.getCode(4,4);
         
         // send the response
         CoapMessage resp=BasicCoapMessageFactory.createResponse(msg,response_code,payload);
         debug("processReceivedRequest(): respond: "+CoapProtocolAnalyzer.analyze(resp).toString());
         bts.respond(resp);
      }
      catch (Exception e)
      {  e.printStackTrace();
      }
   }

   
   /** When the notifier is cancelled by the observer.
    * @param notifier the notifier */   
   private void processNotificationCancelled(Notifier notifier)
   {  notifiers.removeElement(notifier);
   }
   
   
   /** Stops the server. */
   public void halt()
   {  coap_provider.halt();
   }


   /** The main method.
    * @param args the arguments passed to this class at command-line */
   public static void main(String[] args)
   {  try
      {  new CoapXServer(args[0],args[1]);
         System.out.println("Press ENTER to quit");
         new BufferedReader(new InputStreamReader(System.in)).readLine();
         System.out.println("closing..");
         System.exit(0);
      }
      catch (Exception e)
      {  e.printStackTrace();
      }
   }
   
}
