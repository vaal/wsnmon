package org.zoolu.coap.observe;





/** It listens for notifier events.
 */
public interface NotifierListener
{
   /** When the notifier is cancelled by the observer.
    * @param notifier the notifier */
   public void onNotificationCancelled(Notifier notifier);

}
