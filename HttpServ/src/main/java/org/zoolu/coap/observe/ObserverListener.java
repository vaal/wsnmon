package org.zoolu.coap.observe;



import org.zoolu.coap.core.CoapMessage;



/** It listens for observer events.
 */
public interface ObserverListener
{
   /** When a new notification is received.
    * @param observer the observer
    * @param resp_code the response code
    * @param state updated representation of the new resource state
    * @param seq_num notification sequence number
    * @param resp the CoAP response message */
   public void onNotification(Observer observer, int resp_code, byte[] state, int seq_num, CoapMessage resp);


   /** When notification service terminate.
    * @param observer the observer
    * @param resp_code the response code
    * @param resp the CoAP response message */
   public void onTermination(Observer observer, int resp_code, CoapMessage resp);


}
