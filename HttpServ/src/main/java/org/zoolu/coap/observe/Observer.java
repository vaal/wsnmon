package org.zoolu.coap.observe;



import org.zoolu.coap.core.*;
import org.zoolu.coap.message.*;
import org.zoolu.coap.option.*;
import org.zoolu.net.SocketAddress;

import java.net.URI;



/** An observer is a CoAP client that is interested in having a current representation of the resource at any given time.
 * <p/>
 * It implements the observer model according to <draft-ietf-core-observe> "Observing Resources in CoAP".
 */
public class Observer
{   
   /** CoAP provider */
   CoapProvider coap_provider;
   
   /** Token */
   CoapTransactionId id=null;
   
   /** Observer listener */
   ObserverListener listener;
   
   

   /** Creates a new observer.
    * @param coap_provider the CoAP provide */
   public Observer(CoapProvider coap_provider, ObserverListener listener)
   {  this.coap_provider=coap_provider;
      this.listener=listener;
   }

   
   /** Creates a new observer.
    * @param server_soaddr the socket address of the server
    * @param resource_uri the resource URI */
   public void register(SocketAddress server_soaddr, URI resource_uri) throws CoapMessageFormatException
   {  CoapTransactionClientListener this_tc_listener=new CoapTransactionClientListener()
      {  public void onTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
         {  processTransactionResponse(tc,resp);
         }
         public void onTransactionFailure(CoapTransactionClient tc)
         {  processTransactionFailure(tc);
         }
      };
      CoapTransactionClient tc=new CoapTransactionClient(coap_provider,server_soaddr,this_tc_listener);
      CoapMessage req=CoapMessageFactory.createCONRequest(CoapMethod.GET,resource_uri);
      req.addOption(new ObserveOption(ObserveOption.REGISTER));
      id=new CoapTransactionId(req.getToken());
      tc.request(req);
   }
   
   
   /** When a CoAP response is received for the pending request. */
   private void processTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
   {  int resp_code=resp.getCode();
      if (resp_code>=CoapResponseCode.getCode(2,0) && resp_code<CoapResponseCode.getCode(3,0))
      {  CoapProviderListener this_cp_listener=new CoapProviderListener()
         {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
            {  processReceivedMessage(coap_provider,msg);
            }
         };
         coap_provider.addListener(id,this_cp_listener);
         byte[] state=resp.getPayload();
         int seq_num=resp.hasOption(CoapOptionNumber.Observe)? new ObserveOption(resp.getOption(CoapOptionNumber.Observe)).getOptionNumber() : -1;
         if (listener!=null) listener.onNotification(this,resp_code,state,seq_num,resp);
      }
      else
      {  if (listener!=null) listener.onTermination(this,resp_code,null);
      }
   }


   /** When a RST is received for a Confirmable request or transaction timeout expired. */
   private void processTransactionFailure(CoapTransactionClient tc)
   {  if (listener!=null) listener.onTermination(this,-1,null);
   }


   /** When a new CoAP message is received.
    * @param coap_provider the CoAP provider
    * @param msg the received CoAP message */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  if (!msg.isResponse()) return;
      // else
      new CoapReliableReception(coap_provider,msg);
      int resp_code=msg.getCode();
      if (resp_code>=CoapResponseCode.getCode(2,0) && resp_code<CoapResponseCode.getCode(3,0))
      {  byte[] state=msg.getPayload();
         int seq_num=new ObserveOption(msg.getOption(CoapOptionNumber.Observe)).getSequenceNumber();
         if (listener!=null) listener.onNotification(this,resp_code,state,seq_num,msg);
      }
      else
      {  if (listener!=null) listener.onTermination(this,resp_code,msg);
      }
   }

}
