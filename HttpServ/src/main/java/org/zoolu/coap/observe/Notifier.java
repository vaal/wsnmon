package org.zoolu.coap.observe;



import org.zoolu.coap.core.BasicCoapMessageFactory;
import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapProvider;
import org.zoolu.coap.core.CoapReliableTransmission;
import org.zoolu.coap.core.CoapReliableTransmissionListener;
import org.zoolu.coap.message.CoapResponseCode;
import org.zoolu.coap.option.ObserveOption;



/** It notifies resource changes to a given observer.
 * <p/>
 * It implements the observer model according to <draft-ietf-core-observe> "Observing Resources in CoAP".
 */
public class Notifier
{
   /** CoAP provider */
   CoapProvider coap_provider;
   
   /** Register request message */
   CoapMessage req;

   /** Sequence number of the last notify */
   int seqn=0;

   /** This reliable transmission listener */
   CoapReliableTransmissionListener this_rt_listener;
   
   /** Notifier listener */
   NotifierListener listener;
   
   
   /** Creates a new Notifier.
    * @param coap_provider the CoAP provider
    * @param req the register request sent by the observer */
   public Notifier(CoapProvider coap_provider, CoapMessage req, NotifierListener listener)
   {  this.coap_provider=coap_provider;
      this.req=req;
      this.listener=listener;
      this_rt_listener=new CoapReliableTransmissionListener()
      {  @Override
         public void onTransmissionAcknowledgement(CoapReliableTransmission reliable_transmission, CoapMessage ack)
         {  // do nothing
         }
         @Override
         public void onTransmissionReject(CoapReliableTransmission reliable_transmission, CoapMessage rst)
         {  cancel();
         }
         @Override
         public void onTransmissionTimeout(CoapReliableTransmission reliable_transmission)
         {  cancel();
         }    
      };
   }

   
   /** Notifies a new state.
    * @param state the new state to be notified */
   public void notify(byte[] state)
   {  CoapMessage resp=BasicCoapMessageFactory.createResponse(req,CoapResponseCode.getCode(2,5),state);
      seqn=(seqn+1)&0xFFFFFF;
      resp.addOption(new ObserveOption(seqn));
      //coap_provider.send(resp,req.getRemoteSoAddress());
      new CoapReliableTransmission(coap_provider,req.getRemoteSoAddress(),this_rt_listener).send(resp);
   }

   
   /** Notifies a new value.
    * @param payload the new value to be notified */
   private void cancel()
   {  if (listener!=null) listener.onNotificationCancelled(this);
   }
   
}
