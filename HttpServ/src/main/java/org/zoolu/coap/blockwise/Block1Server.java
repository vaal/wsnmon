package org.zoolu.coap.blockwise;



import java.util.Vector;

import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.coap.core.*;
import org.zoolu.coap.message.CoapResponseCode;
import org.zoolu.coap.option.Block1Option;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.util.ByteUtils;



/** Server-side support for blockwise transfers from client to server (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
class Block1Server
{
   /** Logs debug message */
   private static void debug(String str)
   {  if (BlockwiseTransactionServer.DEBUG) System.out.println("DEBUG: Block1Server: "+str);
   }
   
   
   /** Max server-side block size */
   int server_max_size=0;

   /** CoAP provider */
   CoapProvider coap_provider;

   /** Request */
   CoapMessage req;

   /** Sequence number of received bytes */
   //long seqn=0;
   
   /** Block buffer */
   BlockBuffer block_buffer=null;

   /** Current block size */
   int current_block_size=-1;

   /** Last block sequence number */
   int last_block_seqn=-1;

   /** CoAP provider listener */
   CoapProviderListener this_cp_listener;
   
   /** Block2Client listener */
   Block1ServerListener listener;

   
   /** Creates a new Block2Server.
    * @param coap_provider the CoAP provider
    * @param req the request message
    * @param server_max_size the maximum size
    * @param listener blockwise server listener */
   public Block1Server(CoapProvider coap_provider, CoapMessage req, int server_max_size, Block1ServerListener listener)
   {  this.coap_provider=coap_provider;
      this.req=req;
      this.server_max_size=server_max_size;
      this.listener=listener;
      this_cp_listener=new CoapProviderListener()
      {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg) {  processReceivedMessage(coap_provider,msg);  }
      };
      coap_provider.addListener(new CoapTransferId(req.getCode(),req.getRemoteSoAddress()),this_cp_listener);
      processReceivedMessage(coap_provider,req);
   }


   /** When a new CoAP message is received. */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  debug("processReceivedMessage(CoapProvider,CoapMessage): "+CoapProtocolAnalyzer.analyze(msg).toString());
      if (!msg.hasOption(CoapOptionNumber.Block1))
      {  debug("processReceivedMessage(): no Block1 option");
         coap_provider.removeListener(this_cp_listener);
         if (listener!=null) listener.onReceivedRequest(this,msg);
      }
      else
      {  Block1Option block1_opt=new Block1Option(msg.getOption(CoapOptionNumber.Block1));
         int block1_seqn=(int)block1_opt.getSequenceNumber();
         int block1_size=block1_opt.getSize();
         byte[] block=msg.getPayload();
         boolean more=block1_opt.moreBlocks();
         debug("processReceivedMessage(): block1_seqn="+block1_seqn+", block1_size="+block1_size+", more="+more);
         if (server_max_size!=0 && server_max_size<block1_size)
         {  debug("processReceivedMessage(): small server_max_size="+server_max_size);
            block1_size=server_max_size;
            debug("processReceivedMessage(): new block buffer");
            block_buffer=new BlockBuffer();
            current_block_size=block1_size;
            last_block_seqn=-1;
            if (block1_seqn==0)
            {  block=ByteUtils.getBytes(block,0,block1_size);
               debug("processReceivedMessage(): add block 0");
               block_buffer.setBlockAt(block,0);
            }
         }
         else
         {  if (block_buffer==null || current_block_size!=block1_size)
            {  debug("processReceivedMessage(): new block buffer");
               block_buffer=new BlockBuffer();
               current_block_size=block1_size;
            }
            debug("processReceivedMessage(): set block "+block1_seqn);
            block_buffer.setBlockAt(block,block1_seqn);
            if (!more) last_block_seqn=block1_seqn;
         }  
         if (block1_seqn!=last_block_seqn || !block_buffer.isFull())
         {  debug("processReceivedMessage(): buffer is not full");
            CoapMessage resp=BasicCoapMessageFactory.createPiggyBackedResponse(msg,CoapResponseCode.getCode(2,31),null);
            resp.addOption(new Block1Option(block1_size,true,block1_seqn));
            debug("processReceivedMessage(): "+CoapProtocolAnalyzer.analyze(resp).toString());
            coap_provider.send(resp,msg.getRemoteSoAddress());
         }
         else
         {  debug("processReceivedMessage(): buffer is full");
            // compose the body and pass it to the user
            byte[] body=block_buffer.getBytes();
            msg.removeOption(CoapOptionNumber.Block1);
            msg.setPayload(body);
            debug("processReceivedMessage(): request has been re-composed");
            if (listener!=null) listener.onReceivedRequest(this,msg);
            listener=null;
            // remove this coap provider listener
            coap_provider.removeListener(this_cp_listener);
         }
      }
   }

}
