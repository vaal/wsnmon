package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.CoapMessage;



/** Listens for server-side blockwise transfer events.
 */
interface Block1ServerListener
{
   /** When a new CoAP request message is received.
    * @param block1_server the blockwise transfer server
    * @param req the received CoAP request */
   public void onReceivedRequest(Block1Server block1_server, CoapMessage req);

}
