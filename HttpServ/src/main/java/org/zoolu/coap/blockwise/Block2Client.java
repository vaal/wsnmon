package org.zoolu.coap.blockwise;



import java.util.Vector;

import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapProvider;
import org.zoolu.coap.core.CoapTransactionClient;
import org.zoolu.coap.core.CoapTransactionClientListener;
import org.zoolu.coap.option.Block2Option;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.net.SocketAddress;



/** Client-side support for blockwise transfers from server to client (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
class Block2Client
{
   /** Logs debug message */
   private static void debug(String str)
   {  if (BlockwiseTransactionClient.DEBUG) System.out.println("DEBUG: Block2Client: "+str);
   }

   /** Max client-side block size */
   int client_max_size=0;

   /** CoapProvider */
   CoapProvider coap_provider;

   /** Remote socket address */
   SocketAddress remote_soaddr;

   /** Request message */
   CoapMessage req;

   /** Sequence number of received bytes */
   long seqn=0;
   
   /** Block buffer */
   Vector<byte[]> block_buffer=new Vector<byte[]>();
   
   /** CoAP transaction client*/
   CoapTransactionClientListener this_tc_listener;
 
   /** Block2Client listener */
   Block2ClientListener listener;



   /** Creates a new Block2Client.
    * @param coap_provider the CoAP provider
    * @param remote_soaddr the socket address of the remote CoAP server
    * @param b2c_listener the listener of this blockwise transfer client */
   public Block2Client(CoapProvider coap_provider, SocketAddress remote_soaddr, Block2ClientListener listener)
   {  init(coap_provider,remote_soaddr,listener);
   }


   /** Creates a new Block2Client.
    * @param coap_provider the CoAP provider
    * @param remote_soaddr the socket address of the remote CoAP server
    * @param req first request message
    * @param resp first response message
    * @param b2c_listener the listener of this blockwise transfer client */
   public Block2Client(CoapProvider coap_provider, SocketAddress remote_soaddr, CoapMessage req, CoapMessage resp, Block2ClientListener listener)
   {  init(coap_provider,remote_soaddr,listener);
      this.req=req;
      processTransactionResponse(null,resp);
   }
  
   
   
   /** Initializes the Block2Client.
    * @param coap_provider the CoAP provider
    * @param remote_soaddr the socket address of the remote CoAP server
    * @param b2c_listener the listener of this blockwise transfer client */
   private void init(CoapProvider coap_provider, SocketAddress remote_soaddr, Block2ClientListener listener)
   {  this.coap_provider=coap_provider;
      this.remote_soaddr=remote_soaddr;
      this.listener=listener;
      this_tc_listener=new CoapTransactionClientListener()
      {  public void onTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
         {  processTransactionResponse(tc,resp);
         }
         public void onTransactionFailure(CoapTransactionClient tc)
         {  processTransactionFailure(tc);
         }
      };
   }


   /** Sets the maximum block size.
    * @param client_max_size
    * @return this object */
   public Block2Client setMaximumBlockSize(int client_max_size)
   {  this.client_max_size=client_max_size;
      return this;
   }


   /** Sends CoAP request.
    * @param req the request message */
   public void request(CoapMessage req)
   {  debug("request(): "+CoapProtocolAnalyzer.analyze(req).toString());
      if (client_max_size>0) req.addOption(new Block2Option(client_max_size,false,0));
      new CoapTransactionClient(coap_provider,remote_soaddr,this_tc_listener).request(this.req=req);
   }


   /** When a CoAP response is received for the pending request. */
   private void processTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
   {  debug("processTransactionResponse(): "+CoapProtocolAnalyzer.analyze(resp).toString());
      if (!resp.hasOption(CoapOptionNumber.Block2))
      {  if (listener!=null) listener.onReceivedResponse(this,resp);
      }
      else
      {  debug("processTransactionResponse(): Block2 option found");
         Block2Option block2_opt=new Block2Option(resp.getOption(CoapOptionNumber.Block2));
         long block2_seqn=block2_opt.getSequenceNumber();
         int size=block2_opt.getSize();
         boolean more=block2_opt.moreBlocks();
         // update the receiver block buffer
         if ((block2_seqn*size)!=seqn)
         {  debug("processTransactionResponse(): seqn mismatch: restart with seqn=0");
            // re-request the first missing block
            req.setMessageId(CoapMessage.pickMessageId());
            req.setToken(CoapMessage.pickToken());
            req.setOption(new Block2Option(size,false,seqn=0));
            new CoapTransactionClient(coap_provider,remote_soaddr,this_tc_listener).request(req);
         }
         else
         {  // update the receiver buffer
            byte[] block=resp.getPayload();
            block_buffer.addElement(block);
            seqn+=block.length;
            if (more)
            {  debug("processTransactionResponse(): request next block");
               // request the next block
               req.setMessageId(CoapMessage.pickMessageId());
               req.setToken(CoapMessage.pickToken());
               req.setOption(new Block2Option(size,false,seqn/size));
               new CoapTransactionClient(coap_provider,remote_soaddr,this_tc_listener).request(req);
            }
            else
            {  debug("processTransactionResponse(): it was the last block: recompose the response");
               // compose the body and pass it to the user
               byte[] body=new byte[(int)seqn];
               int index=0;
               for (int i=0; i<block_buffer.size(); i++)
               {  byte[] block_i=(byte[])block_buffer.elementAt(i);
                  for (int k=0; k<block_i.length; k++) body[index++]=block_i[k];
               }
               resp.removeOption(CoapOptionNumber.Block2);
               resp.setPayload(body);
               if (listener!=null) listener.onReceivedResponse(this,resp);
            }
         }
      }
   }


   /** When a RST is received for a Confirmable request or transaction timeout expired. */
   private void processTransactionFailure(CoapTransactionClient tc)
   {  debug("processTransactionFailure()");
   }

}
