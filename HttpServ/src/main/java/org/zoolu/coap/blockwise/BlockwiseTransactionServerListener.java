package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapTransactionServer;



/** Listens for server-side blockwise transfer events.
 */
public interface BlockwiseTransactionServerListener
{
   /** When a new CoAP request message is received.
    * @param bts the blockwise transaction server
    * @param req the received CoAP request */
   public void onReceivedRequest(BlockwiseTransactionServer bts, CoapMessage req);

   /** When a RST is received for a Confirmable response or transaction timeout expired.
    * @param bts the blockwise transaction server */
   public void onTransactionFailure(CoapTransactionServer bts);

}
