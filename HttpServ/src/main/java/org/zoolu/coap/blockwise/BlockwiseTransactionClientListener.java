package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.CoapMessage;



/** Listens for client-side blockwise transfer events.
 */
public interface BlockwiseTransactionClientListener
{
   /** When a CoAP response message is received for the pending request.
    * @param btc the blockwise transaction client
    * @param msg the received CoAP response */
   public void onTransactionResponse(BlockwiseTransactionClient btc, CoapMessage resp);

   /** When a RST is received for a Confirmable request or transaction timeout expired.
    * @param btc the blockwise transaction client */
   public void onTransactionFailure(BlockwiseTransactionClient btc);

}
