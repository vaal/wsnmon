package org.zoolu.coap.blockwise;



import java.util.Vector;

import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapProvider;
import org.zoolu.coap.core.CoapTransactionClient;
import org.zoolu.coap.core.CoapTransactionClientListener;
import org.zoolu.coap.option.Block1Option;
import org.zoolu.coap.option.Block2Option;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.net.SocketAddress;
import org.zoolu.util.ByteUtils;



/** Client-side support for blockwise transfers from client to server (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
class Block1Client
{
   /** Logs debug message */
   private static void debug(String str)
   {  if (BlockwiseTransactionClient.DEBUG) System.out.println("DEBUG: Block1Client: "+str);
   }

   /** Max client-side block size */
   int client_max_size=0;

   /** CoAP provider */
   CoapProvider coap_provider;

   /** Remote socket address */
   SocketAddress remote_soaddr;

   /** Request message */
   CoapMessage req;

   /** Sequence number of received bytes */
   long seqn=0;
   
   /** Request body */
   byte[] req_body;

   /** CoAP transaction client listener */
   CoapTransactionClientListener this_tc_listener;
 
   /** Block2Client listener */
   Block1ClientListener listener;



   /** Creates a new Block1Client.
    * @param coap_provider the CoAP provider
    * @param remote_soaddr the socket address of the remote CoAP server
    * @param b2c_listener the listener of this blockwise transfer client */
   public Block1Client(CoapProvider coap_provider, SocketAddress remote_soaddr, Block1ClientListener listener)
   {  this.coap_provider=coap_provider;
      this.remote_soaddr=remote_soaddr;
      this.listener=listener;
      this_tc_listener=new CoapTransactionClientListener()
      {  public void onTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
         {  processTransactionResponse(tc,resp);
         }
         public void onTransactionFailure(CoapTransactionClient tc)
         {  processTransactionFailure(tc);
         }
      };
   }


   /** Sets the maximum block size.
    * @param client_max_size
    * @return this object */
  public Block1Client setMaximumBlockSize(int client_max_size)
  {  this.client_max_size=client_max_size;
     return this;
  }


   /** Sends CoAP request.
    * @param req the request message */
   public void request(CoapMessage req)
   {  req_body=req.getPayload();
      if (client_max_size>0)
      {  if (client_max_size<req_body.length)
         {  byte[] block_0=ByteUtils.getBytes(req_body,0,client_max_size);
            req.setPayload(block_0);
            req.addOption(new Block1Option(client_max_size,true,0));
         }
         else req.addOption(new Block1Option(client_max_size,false,0));
      }
      new CoapTransactionClient(coap_provider,remote_soaddr,this_tc_listener).request(this.req=req);
   }


   /** When a CoAP response is received for the pending request. */
   private void processTransactionResponse(CoapTransactionClient tc, CoapMessage resp)
   {  debug("processTransactionResponse(): "+CoapProtocolAnalyzer.analyze(resp).toString());
      if (!resp.hasOption(CoapOptionNumber.Block1))
      {  debug("processTransactionResponse(): no Block1 option");
         debug("processTransactionResponse(): listener: "+listener);
         if (listener!=null) listener.onReceivedResponse(this,resp);
      }
      else
      {  Block2Option block1_opt=new Block2Option(resp.getOption(CoapOptionNumber.Block1));
         long block1_seqn=block1_opt.getSequenceNumber();
         int block1_size=block1_opt.getSize();
         boolean more=block1_opt.moreBlocks();
         debug("processTransactionResponse(): block1_seqn="+block1_seqn+", block1_size="+block1_size+", more="+more);
         if (block1_size*block1_seqn==seqn)
         {  debug("processTransactionResponse(): seqn number match");
            seqn+=block1_size;
            if (seqn+block1_size<req_body.length)
            {  debug("processTransactionResponse(): more blocks to send");
               byte[] block_i=ByteUtils.getBytes(req_body,(int)seqn,block1_size);
               req.setPayload(block_i);
               req.setOption(new Block1Option(block1_size,true,(int)(seqn/block1_size)));
               new CoapTransactionClient(coap_provider,remote_soaddr,this_tc_listener).request(req);
            }
            else
            if (seqn<req_body.length)
            {  debug("processTransactionResponse(): only one other block to send ("+(req_body.length-(int)seqn)+"B)");
               byte[] block_i=ByteUtils.getBytes(req_body,(int)seqn,req_body.length-(int)seqn);
               req.setPayload(block_i);
               req.setOption(new Block1Option(block1_size,false,(int)(seqn/block1_size)));
               new CoapTransactionClient(coap_provider,remote_soaddr,this_tc_listener).request(req);
            }
            else
            {  // error..
               debug("processTransactionResponse(): no final response without Block1 option has been received");
            }
         }
         else
         {  // error..
            debug("processTransactionResponse(): sequence number mismatching");
         }
      }
   }


   /** When a RST is received for a Confirmable request or transaction timeout expired. */
   private void processTransactionFailure(CoapTransactionClient tc)
   {  debug("processTransactionFailure()");
   }

}
