package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.CoapMessage;



/** Listens for client-side blockwise transfer events.
 */
interface Block2ClientListener
{
   /** When a new CoAP response message is received.
    * @param block2_client the blockwise transfer client
    * @param msg the received CoAP response */
   public void onReceivedResponse(Block2Client block2_client, CoapMessage resp);

}
