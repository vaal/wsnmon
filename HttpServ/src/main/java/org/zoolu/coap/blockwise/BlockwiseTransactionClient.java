package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapProvider;
import org.zoolu.coap.option.Block2Option;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.net.SocketAddress;




/** Client-side support for blockwise transfers (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
public class BlockwiseTransactionClient
{
   /** Debug mode */
   static boolean DEBUG=false;

   /** Max client-side block size */
   int client_max_size=0;

   /** CoAP provider */
   CoapProvider coap_provider;

   /** Remote socket address */
   SocketAddress remote_soaddr;

   /** Request message */
   CoapMessage req;
   
   /** Blockwise transaction client listener */
   BlockwiseTransactionClientListener listener;
   
   
   /** Creates a new BlockwiseTransactionClient.
    * @param coap_provider the CoAP provider
    * @param remote_soaddr the socket address of the remote CoAP server
    * @param listener the listener of this blockwise transaction client */
   public BlockwiseTransactionClient(CoapProvider coap_provider, SocketAddress remote_soaddr, BlockwiseTransactionClientListener listener)
   {  this.coap_provider=coap_provider;
      this.remote_soaddr=remote_soaddr;
      this.listener=listener;
   }

   
   /** Sets the maximum block size.
    * @param client_max_size
    * @return this object */
   public BlockwiseTransactionClient setMaximumBlockSize(int client_max_size)
   {  this.client_max_size=client_max_size;
      return this;
   }


   /** Sends CoAP request.
    * @param req the request message */
   public void request(CoapMessage req)
   {  this.req=req;
      if (client_max_size>0) req.addOption(new Block2Option(client_max_size,false,0));
      Block1ClientListener this_b1c_listener=new Block1ClientListener()
      {  @Override
         public void onReceivedResponse(Block1Client block1_client, CoapMessage resp)
         {  processReceivedResponse(block1_client,resp);
         }
      };
      new Block1Client(coap_provider,remote_soaddr,this_b1c_listener).setMaximumBlockSize(client_max_size).request(req);
   }
   
   
   /** When a new CoAP response message is received.
    * @param block1_client the blockwise transfer client
    * @param msg the received CoAP response */
   private void processReceivedResponse(Block1Client block1_client, CoapMessage resp)
   {  req.removeOption(CoapOptionNumber.Block1);
      req.setPayload(null);
      Block2ClientListener this_b2c_listener=new Block2ClientListener()
      {  @Override
         public void onReceivedResponse(Block2Client block2_client, CoapMessage resp)
         {  processReceivedResponse(block2_client,resp);
         }
      };
      new Block2Client(coap_provider,resp.getRemoteSoAddress(),req,resp,this_b2c_listener);
   }

   
   /** When a new CoAP response message is received.
    * @param block2_client the blockwise transfer client
    * @param msg the received CoAP response */
   private void processReceivedResponse(Block2Client block2_client, CoapMessage resp)
   {  if (listener!=null) listener.onTransactionResponse(this,resp);
      listener=null;
   }

}
