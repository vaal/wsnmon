package org.zoolu.coap.blockwise;



import org.zoolu.coap.analyzer.CoapProtocolAnalyzer;
import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapOption;
import org.zoolu.coap.core.CoapProvider;
import org.zoolu.coap.core.CoapProviderListener;
import org.zoolu.coap.core.CoapTransactionServer;
import org.zoolu.coap.core.CoapTransferId;
import org.zoolu.coap.option.Block2Option;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.util.ByteUtils;



/** Server-side support for blockwise transfers from server to client  (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
public class Block2Server
{
   /** Logs debug message */
   private static void debug(String str)
   {  if (BlockwiseTransactionServer.DEBUG) System.out.println("DEBUG: Block2Server: "+str);
   }

   
   /** Max server-side block size */
   int server_max_size=0;

   /** CoAP provider */
   CoapProvider coap_provider;

   /** Request */
   CoapMessage req=null;

   /** Composed response */
   CoapMessage resp=null;

   /** CoAP provider listener */
   CoapProviderListener this_cp_listener;

   
   
   /** Creates a new Block2Server. */
   public Block2Server()
   {  
   }


   /** Creates a new Block2Server.
    * @param server_max_size the maximum size */
   public Block2Server(CoapProvider coap_provider, CoapMessage req, int server_max_size)
   {  debug("Block2Server()");
      this.coap_provider=coap_provider;
      this.req=req;
      this.server_max_size=server_max_size;
      
      this_cp_listener=new CoapProviderListener()
      {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg) {  processReceivedMessage(coap_provider,msg);  }
      };
      coap_provider.addListener(new CoapTransferId(req.getCode(),req.getRemoteSoAddress()),this_cp_listener);
   }


   /** When a new CoAP message is received. */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  debug("processReceivedMessage(): "+CoapProtocolAnalyzer.analyze(msg).toString());
      if (resp!=null) respond(msg,resp);
   }

   
   /** Responds to the request.
    * @param resp a NON-blockwise response message */
   public void respond(CoapMessage resp)
   {  debug("respond()");
      this.resp=resp;
      respond(req,resp);
   }
   
   
   /** Responds to the request.
    * @param resp a NON-blockwise response message */
   private void respond(CoapMessage req, CoapMessage resp)
   {  resp=blockwiseResponse(req,new CoapMessage(resp),server_max_size);
      debug("respond(): "+CoapProtocolAnalyzer.analyze(resp).toString());
      CoapOption block2_opt=resp.getOption(CoapOptionNumber.Block2);
      if (block2_opt==null || !new Block2Option(block2_opt).moreBlocks()) coap_provider.removeListener(this_cp_listener);
      new CoapTransactionServer(coap_provider,req,null).respond(resp); 
   }
   
   
   /** Makes a response blockwise compatible.
    * @param req the request message
    * @param resp a NON-blockwise response message
    * @return the blockwise compatible response */
   public static CoapMessage blockwiseResponse(CoapMessage req, CoapMessage resp, int server_max_size)
   {  byte[] payload=resp.getPayload();
      if (payload!=null)
      {  if (req.hasOption(CoapOptionNumber.Block2))
         {  // client max size
            Block2Option req_block2opt=new Block2Option(req.getOption(CoapOptionNumber.Block2));
            int size=req_block2opt.getSize();
            int seqn=(int)req_block2opt.getSequenceNumber();
            if (server_max_size>0 && size>server_max_size)
            {  size=server_max_size;
               seqn=0;
            }
            int offset=seqn*size;
            boolean more=payload.length>(offset+size);
            payload=ByteUtils.getBytes(payload,offset,(more)?size:payload.length-offset);
            resp.setPayload(payload);
            resp.addOption(new Block2Option(size,more,seqn));
         }
         else
         if (server_max_size>0)
         {  // server max size
            if (payload.length>server_max_size)
            {  payload=ByteUtils.getBytes(payload,0,server_max_size);
               resp.setPayload(payload);
               resp.addOption(new Block2Option(server_max_size,true,0));
            }
            else
            {  resp.addOption(new Block2Option(server_max_size,false,0));
            }
         }
      }
      return resp;
   }

}
