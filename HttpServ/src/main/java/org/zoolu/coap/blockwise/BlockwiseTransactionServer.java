package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.*;



/** Server-side support for blockwise transfers (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
public class BlockwiseTransactionServer
{
   /** Debug mode */
   static boolean DEBUG=false;

   /** Max server-side block size */
   int server_max_size=0;

   /** CoAP provider */
   CoapProvider coap_provider;
   
   /** Composed request */
   CoapMessage req=null;
   
   /** Composed response */
   //CoapMessage resp=null;

   /** Listener */
   BlockwiseTransactionServerListener listener;


   
   /**Creates a new BlockwiseTransactionServer.
    * @param coap_provider the CoAP provider
    * @param req the request message
    * @param server_max_size the maximum size
    * @param listener blockwise transaction server listener */
   public BlockwiseTransactionServer(CoapProvider coap_provider, CoapMessage req, int server_max_size, BlockwiseTransactionServerListener listener)
   {  this.coap_provider=coap_provider;
      this.server_max_size=server_max_size;
      this.listener=listener;
      Block1ServerListener this_b1s_listener=new Block1ServerListener()
      {  public void onReceivedRequest(Block1Server block1_server, CoapMessage req)
         {  processReceivedRequest(block1_server,req);
         }
      };
      new Block1Server(coap_provider,req,server_max_size,this_b1s_listener);
   }
   
   
   /** When a new CoAP request message is completely received (transferred).
    * @param block1_server the blockwise transfer server
    * @param req the received CoAP request */
   private synchronized void processReceivedRequest(Block1Server block1_server, CoapMessage req)
   {  this.req=req;
      if (listener!=null) listener.onReceivedRequest(this,req);
   }
   

   /** Sends CoAP response.
    * @param resp the response message */
   public synchronized void respond(CoapMessage resp)
   {  //this.resp=resp;
      new Block2Server(coap_provider,req,server_max_size).respond(resp);
   }
   
}
