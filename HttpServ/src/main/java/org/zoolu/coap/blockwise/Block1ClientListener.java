package org.zoolu.coap.blockwise;



import org.zoolu.coap.core.CoapMessage;



/** Listens for client-side blockwise transfer events.
 */
interface Block1ClientListener
{
   /** When a new CoAP response message is received.
    * @param block1_client the blockwise transfer client
    * @param msg the received CoAP response */
   public void onReceivedResponse(Block1Client block1_client, CoapMessage resp);

}
