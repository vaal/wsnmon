/*
 * Copyright (C) 2013 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.message;




/** Class CoapResponseCode collects static methods and attributes for dealing with CoAP responses.
  */
public class CoapResponseCode
{
   /** Gets response code description.
     * @return response description */
   public static String getDescription(int response_code)
   {  switch(response_code)
      {  case 65 : return "2.01 Created";
         case 66 : return "2.02 Deleted";
         case 67 : return "2.03 Valid";
         case 68 : return "2.04 Changed";
         case 69 : return "2.05 Content";
         case 95 : return "2.31 Continue";
         
         case 128 : return "4.00 Bad Request";
         case 129 : return "4.01 Unauthorized";
         case 130 : return "4.02 Bad Option";
         case 131 : return "4.03 Forbidden";
         case 132 : return "4.04 Not Found";
         case 133 : return "4.05 Method Not Allowed";
         case 134 : return "4.06 Not Acceptable";
         case 136 : return "4.08 Request Entity Incomplete"; 
         case 140 : return "4.12 Precondition Failed";
         case 141 : return "4.13 Request Entity Too Large";
         case 143 : return "4.15 Unsupported Content-Format";

         case 160 : return "5.00 Internal Server Error";
         case 161 : return "5.01 Not Implemented";
         case 162 : return "5.02 Bad Gateway";
         case 163 : return "5.03 Service Unavailable";
         case 164 : return "5.04 Gateway Timeout";
         case 165 : return "5.05 Proxing Not Supported";
      }
      // else
      if (response_code>=64 && response_code<192) return String.valueOf((response_code>>5)&0x7)+"."+String.valueOf(response_code&0x1f);
      // else
      return null;
   }


   /** Composes the response code with a given code class and given detail.
     * @return the specified response code */
   public static int getCode(int code_class, int code_detail)
   {  return ((code_class&0x7)<<5) | (code_detail&0x1f);   
   }

}
