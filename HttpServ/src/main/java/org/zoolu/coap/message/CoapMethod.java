/*
 * Copyright (C) 2013 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.message;




/** Class CoapMethod collects static methods and attributes for dealing with CoAP requests.
  */
public class CoapMethod
{
   
   private int code;
   private CoapMethod(int code){
	   this.code = code;
   }
   
   private static final int _GET = 1;
   private static final int _POST = 2;
   private static final int _PUT = 3;
   private static final int _DELETE = 4;
   
   public static final CoapMethod GET = new CoapMethod(_GET);
   public static final CoapMethod POST = new CoapMethod(_POST);
   public static final CoapMethod PUT = new CoapMethod(_PUT);
   public static final CoapMethod DELETE = new CoapMethod(_DELETE);

   /** Gets code.
    * @return method code */
   public int getCode()
   {  return code;
   }
  
   /** Gets method name.
     * @return method name (GET, POST,  PUT, or DELETE) */
   public static String getName(CoapMethod method)
   {  switch(method.code)
      {  case _GET : return "GET";
         case _POST : return "POST";
         case _PUT : return "PUT";
         case _DELETE : return "DELETE";
      }
      // else
      return null;
   }
   
   public String toString(){
	   return getName(this);
   }

   /** Gets method code from method name.
     * @return method code (1=GET, 2=POST,  3=PUT, 4=DELETE) */
   public static CoapMethod getCodeByName(String method_name)
   {  if (method_name.equalsIgnoreCase("GET")) return GET;
      // else
      if (method_name.equalsIgnoreCase("POST")) return POST;
      // else
      if (method_name.equalsIgnoreCase("PUT")) return PUT;
      // else
      if (method_name.equalsIgnoreCase("DELETE")) return DELETE;
      // else
      return null;
   }


	@Override
	public boolean equals(Object obj) {
		if(obj instanceof CoapMethod){
			CoapMethod method = (CoapMethod)obj;
			return method.code == this.code;
		}
		return false;
	}
   
	/** Gets method by code.
     * @return method (GET, POST,  PUT, DELETE) */
   public static CoapMethod getMethodByCode(int code)
   {  switch(code)
	      {  case _GET : return GET;
	         case _POST : return POST;
	         case _PUT : return PUT;
	         case _DELETE : return DELETE;
	      }
	      // else
	      return null;
   }
    

}
