package org.zoolu.coap.message;



import org.zoolu.coap.core.*;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.coap.option.ContentFormatOption;



/** CoAP response message, with method for handling CoAP-specific options.
 */
public class CoapResponse extends CoapMessage
{

   /** Creates a new CoapResponse.
    * @param msg a CoAP message to be copied */
   public CoapResponse(CoapMessage msg)
   {  super(msg);
   }


   /** Creates a new CoapResponse.
    * @param type message type (Confirmable (0), Non-Confirmable (1), Acknowledgement (2) or Reset (3))
    * @param code request method (1-31) or response code (64-191), or 0 for empty message
    * @param message_id message ID, used for the detection of message duplication, and to match messages of type Acknowledgement/Reset to messages of type Confirmable/Non-confirmable */
   public CoapResponse(short type, int code, int message_id)
   {  super(type,code,message_id);
   }


   /** Creates a new CoapResponse.
    * @param type message type (Confirmable (0), Non-Confirmable (1), Acknowledgement (2) or Reset (3))
    * @param code request method (1-31) or response code (64-191), or 0 for empty message.
    * @param message_id message ID, used for the detection of message duplication, and to match messages of type Acknowledgement/Reset to messages of type Confirmable/Non-confirmable
    * @param token the token used to correlate requests and responses (if any)
    * @param options array of message options (if any)
    * @param payload message payload */
   /*public CoapResponse(short type, int code, int message_id, byte[] token, CoapOption[] options, byte[] payload)
   {  super(type,code,message_id,token,options,payload);
   }*/


   /** Creates a new CoapResponse.
    * @param type message type (Confirmable (0), Non-Confirmable (1), Acknowledgement (2) or Reset (3))
    * @param code request method (1-31) or response code (64-191), or 0 for empty message.
    * @param message_id message ID, used for the detection of message duplication, and to match messages of type Acknowledgement/Reset to messages of type Confirmable/Non-confirmable
    * @param token the token used to correlate requests and responses (if any)
    * @param options list of message options (if any)
    * @param payload message payload */
   /*public CoapResponse(short type, int code, int message_id, byte[] token, List<CoapOption> options, byte[] payload)
   {  super(type,code,message_id,token,options,payload);
   }*/


   /** Sets the location options (Location-Path and Location-Query options).
     * @param location resource location
     * @return this message */
   public CoapResponse setLocation(String location)
   {  String[] location_components=location.split("?");
      String path=null;
      String query=null;
      if (location_components.length==2)
      {  path=location_components[0];
         query=location_components[1];
      }
      else
      if (location_components.length==1)
      {  if (location_components[0].startsWith("/")) path=location_components[0];
         else query=location_components[0];
      }
      return setLocation(path,query);
   }

   
   /** Sets the location options (Location-Path and Location-Query options).
    * @param path the path part of the location
    * @param query the query part of the location
    * @return this message */
  public CoapResponse setLocation(String path, String query)
  {  if (path!=null && path.length()>0)
     {  String[] path_components=path.substring(1).split("/");
        for (int i=0; path_components!=null && i<path_components.length; i++)
        {  addOption(new CoapOption(CoapOptionNumber.LocationPath,path_components[i]));
        }
     }
     if (query!=null && query.length()>0)
     {  String[] query_components=query.split("&");
        for (int i=0; query_components!=null && i<query_components.length; i++)
        {  addOption(new CoapOption(CoapOptionNumber.LocationQuery,query_components[i]));
        }
     }
     return this;
   }

  
   /** Gets the location of the resource (from Location-Path and Location-Query options).
     * @return the location */
   public String getLocation()
   {  StringBuffer sb=new StringBuffer();
      // path
      CoapOption[] path_opt=getOptions(CoapOptionNumber.LocationPath);
      if (path_opt!=null)
      {  for (int i=0; i<path_opt.length; i++) sb.append('/').append(path_opt[i]);
      }
      // query
      CoapOption[] query_opt=getOptions(CoapOptionNumber.LocationQuery);
      if (query_opt!=null)
      {  sb.append('?').append(query_opt[0]);
         for (int i=1; i<query_opt.length; i++) sb.append('&').append(query_opt[i]);
      }
      if (sb.length()>0) return sb.toString();
      // else
      return null;
   }

   
   /** Gets the location path of the resource (from Location-Path option).
    * @return the location path */
   public String getLocationPath()
   {  CoapOption[] path_opt=getOptions(CoapOptionNumber.LocationPath);
      if (path_opt!=null)
      {  StringBuffer sb=new StringBuffer();
         for (int i=0; i<path_opt.length; i++) sb.append('/').append(path_opt[i]);
         return sb.toString();
      }
      return null;
   }

  
   /** Sets content format.
    * @param contet_format content format identifier
    * @return this request */
   public CoapResponse setContentFormat(int content_format)
   {  setOption(new ContentFormatOption(content_format));
      return this;
   }

  
   /** Gets content format.
    * @return contet_format content format identifier */
   public int getContentFormat()
   {  CoapOption cf_opt=getOption(CoapOptionNumber.ContentFormat);
      if (cf_opt!=null) return new ContentFormatOption(cf_opt).getContentFormatIdentifier();
      else return -1;
   }

   
   /** Gets message code as string.
    * @return "empty" for empty message, "GET" for GET request, "POST" for POST request, "PUT" for PUT request, "DELETE" for DELETE request, or response code descrition for responses */
   @Override
   public String getCodeAsString()
   {  return CoapResponseCode.getDescription(getCode());
   }


   /** Gets a string representation of the message options.
    * @return a string of all message options */
   @Override
   protected synchronized String getOptionsAsString()
   {  StringBuffer sb=new StringBuffer();
      CoapOption[] options=this.getOptions();
      for (int i=0; i<options.length; i++)
      {  if (i>0) sb.append(',');
         sb.append(CoapOptionNumber.getOptionName(options[i].getOptionNumber()));
      }
      return sb.toString();
   }

}
