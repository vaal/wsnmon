package org.zoolu.coap.message;



import org.zoolu.coap.core.*;

import java.net.URI;



/** Collects some static methods for simplifying the creation of CoAP messages.
 */
public class CoapMessageFactory extends BasicCoapMessageFactory
{

   /** Creates a new CoAP request.
     * @param method the method code (GET, POST, PUT, or DELETE)
     * @param request_uri the resource URI
     * @param payload the message payload
     * @return the new request message */
   public static CoapRequest createCONRequest(CoapMethod method, URI request_uri)
   {  CoapMessage req=BasicCoapMessageFactory.createRequest(true,method.getCode(),null,null);
      return new CoapRequest(req).setRequestURI(request_uri);
   }

  
   /** Creates a new CoAP request.
    * @param method the method code (GET, POST, PUT, or DELETE)
    * @param request_uri the resource URI
    * @param payload the message payload
    * @return the new request message */
   public static CoapRequest createNONRequest(CoapMethod method, URI request_uri)
   {  CoapMessage req=BasicCoapMessageFactory.createRequest(false,method.getCode(),null,null);
      return new CoapRequest(req).setRequestURI(request_uri);
   }

 
   /** Creates a new CoAP GET request.
     * @param */
   /*public static BasicCoapMessage createGET(boolean confirmable, URI request_uri)
   {  CoapOption[] options=getUriOptions(request_uri);
      BasicCoapMessage req=createRequest(confirmable,CoapMethod.GET,options,null);
      return req;
   }*/

   
   /** Creates a new CoAP PUT request.
    * @param */
   /*public static BasicCoapMessage createPUT(boolean confirmable, URI request_uri, byte[] payload)
   {  CoapOption[] options=getUriOptions(request_uri);
      BasicCoapMessage req=createRequest(confirmable,CoapMethod.PUT,options,payload);
      return req;
   }*/

  
   /** Creates a new CoAP POST request.
    * @param */
   /*public static BasicCoapMessage createPOST(boolean confirmable, URI request_uri, byte[] payload)
   {  CoapOption[] options=getUriOptions(request_uri);
      BasicCoapMessage req=createRequest(confirmable,CoapMethod.POST,options,payload);
      return req;
   }*/

 
   /** Creates a new CoAP DELETE request.
     * @param */
   /*public static BasicCoapMessage createDELETE(boolean confirmable, URI request_uri)
   {  CoapOption[] options=getUriOptions(request_uri);
      BasicCoapMessage req=createRequest(confirmable,CoapMethod.DELETE,options,null);
      return req;
   }*/
   
}
