package org.zoolu.coap.message;



import org.zoolu.coap.core.*;
import org.zoolu.coap.option.CoapOptionNumber;
import org.zoolu.coap.option.ContentFormatOption;
import org.zoolu.coap.option.UriPathOption;
import org.zoolu.coap.option.UriPortOption;
import org.zoolu.coap.option.UriQueryOption;
import org.zoolu.util.ByteUtils;

import java.net.URI;
import java.net.URISyntaxException;



/** CoAP request message, with method for handling CoAP-specific options.
 */
public class CoapRequest extends CoapMessage
{

   /** Creates a new CoapRequest.
    * @param msg a CoAP message to be copied */
   public CoapRequest(CoapMessage msg)
   {  super(msg);
   }


   /** Creates a new CoapRequest.
    * @param type message type (Confirmable (0), Non-Confirmable (1), Acknowledgement (2) or Reset (3))
    * @param code request method (1-31)
    * @param message_id message ID, used for the detection of message duplication, and to match messages of type Acknowledgement/Reset to messages of type Confirmable/Non-confirmable */
   public CoapRequest(short type, int code, int message_id)
   {  super(type,code,message_id);
   }


   /** Creates a new CoapRequest.
    * @param type message type (Confirmable (0), Non-Confirmable (1), Acknowledgement (2) or Reset (3))
    * @param code request method (1-31)
    * @param message_id message ID, used for the detection of message duplication, and to match messages of type Acknowledgement/Reset to messages of type Confirmable/Non-confirmable
    * @param token the token used to correlate requests and responses (if any)
    * @param options array of message options (if any)
    * @param payload message payload */
   /*public CoapRequest(short type, int code, int message_id, byte[] token, CoapOption[] options, byte[] payload)
   {  super(type,code,message_id,token,options,payload);
   }*/


   /** Creates a new CoapRequest.
    * @param type message type (Confirmable (0), Non-Confirmable (1), Acknowledgement (2) or Reset (3))
    * @param code request method (1-31)
    * @param message_id message ID, used for the detection of message duplication, and to match messages of type Acknowledgement/Reset to messages of type Confirmable/Non-confirmable
    * @param token the token used to correlate requests and responses (if any)
    * @param options list of message options (if any)
    * @param payload message payload */
   /*public CoapRequest(short type, int code, int message_id, byte[] token, List<CoapOption> options, byte[] payload)
   {  super(type,code,message_id,token,options,payload);
   }*/


   /** Whether it is a GET request.
    * @return <i>true</i> in case of a GET request */
   public boolean isGET()
   {  return getCode()==CoapMethod.GET.getCode();
   }

 
   /** Whether it is a POST request.
    * @return <i>true</i> in case of a POST request */
  public boolean isPOST()
  {  return getCode()==CoapMethod.POST.getCode();
  }


   /** Whether it is a PUT request.
    * @return <i>true</i> in case of a PUT request */
   public boolean isPUT()
   {  return getCode()==CoapMethod.PUT.getCode();
   }
 

 /** Whether it is a DELETE request.
   * @return <i>true</i> in case of a DELETE request */
 public boolean isDELETE()
 {  return getCode()==CoapMethod.DELETE.getCode();
 }


   /** Sets the target resource URI options (Uri-Host, Uri-Port, Uri-Path, and Uri-Query options).
     * @param uri the URI of the target resource
     * @return this message
     * @throws CoapMessageFormatException */
   public CoapRequest setRequestURI(URI uri) throws CoapMessageFormatException
   {  if (uri.getFragment()!=null) throw new CoapMessageFormatException("URI cannot have a <fragment> component");
      // else
      String host=uri.getHost();
      int port=uri.getPort();
      String path=uri.getPath();
      String query=uri.getQuery();
      return setRequestURI(host,port,path,query);
   }


  /** Sets the target resource URI options (Uri-Host, Uri-Port, Uri-Path, and Uri-Query options).
     * @param host the URI host, that is the Internet host of the resource being requested
     * @param port the URI port, that is the transport-layer port number of the resource
     * @param path the URI path, that is the absolute path to the resource
     * @param query the URI query, that is the query of resource parameters
     * @return this message */
   public CoapRequest setRequestURI(String host, int port, String path, String query)
   {  if (host!=null && host.length()>0) addOption(new CoapOption(CoapOptionNumber.UriHost,host));
      if (port>0) addOption(new UriPortOption(port));
      if (path!=null && path.length()>0)
      {  String[] path_components=path.substring(1).split("/");
         for (int i=0; path_components!=null && i<path_components.length; i++)
         {  addOption(new UriPathOption(path_components[i]));
         }
      }
      if (query!=null)
      {  String[] query_components=query.split("&");
         for (int i=0; query_components!=null && i<query_components.length; i++)
         {  addOption(new UriQueryOption(query_components[i]));
         }
      }
      return this;
   }


   /** Gets the target resource URI (from Uri-Host, Uri-Port, Uri-Path, and Uri-Query options).
     * @return the URI */
   public URI getRequestUri() throws URISyntaxException
   {  // host
      CoapOption host_opt=getOption(CoapOptionNumber.UriPort);
      String host=host_opt!=null? host_opt.getValueAsString() : null;
      // port
      CoapOption port_opt=getOption(CoapOptionNumber.UriPort);
      int port=port_opt!=null? (int)port_opt.getValueAsUnit() : -1;
      // path
      CoapOption[] path_opt=getOptions(CoapOptionNumber.UriPath);
      String path=null;
      if (path_opt!=null)
      {  StringBuffer sb=new StringBuffer();
         for (int i=0; i<path_opt.length; i++) sb.append('/').append(path_opt[i]);
         path=sb.toString();
      }
      // query
      CoapOption[] query_opt=getOptions(CoapOptionNumber.UriQuery);
      String query=null;
      if (query_opt!=null)
      {  StringBuffer sb=new StringBuffer().append('?').append(query_opt[0]);
         for (int i=1; i<query_opt.length; i++) sb.append('&').append(query_opt[i]);
         query=sb.toString();
      }
      return new URI("coap",null,host,port,path,query,null);
   }


   /** Gets the target resource path (from Uri-Path option).
    * @return the path */
   public String getRequestUriPath()
   {  CoapOption[] path_opt=getOptions(CoapOptionNumber.UriPath);
      if (path_opt!=null)
      {  StringBuffer sb=new StringBuffer();
         for (int i=0; i<path_opt.length; i++) sb.append('/').append(path_opt[i]);
         return sb.toString();
      }
      else return null;
   }
   
   
   /** Sets content format.
   * @param contet_format content format identifier
   * @return this request */
   public CoapRequest setContentFormat(int content_format)
   {  setOption(new ContentFormatOption(content_format));
      return this;
   }

   
   /** Gets content format.
   * @return contet_format content format identifier */
   public int getContentFormat()
   {  CoapOption cf_opt=getOption(CoapOptionNumber.ContentFormat);
      if (cf_opt!=null) return new ContentFormatOption(cf_opt).getContentFormatIdentifier();
      else return -1;
   }

   
   /** Gets message code as string.
    * @return "empty" for empty message, "GET" for GET request, "POST" for POST request, "PUT" for PUT request, "DELETE" for DELETE request, or response code descrition for responses */
   @Override
   public String getCodeAsString()
   {  return CoapMethod.getMethodByCode(getCode()).toString();
   }


   /** Gets a string representation of the message options.
    * @return a string of all message options */
   @Override
   protected synchronized String getOptionsAsString()
   {  StringBuffer sb=new StringBuffer();
      CoapOption[] options=this.getOptions();
      for (int i=0; i<options.length; i++)
      {  if (i>0) sb.append(',');
         sb.append(CoapOptionNumber.getOptionName(options[i].getOptionNumber()));
      }
      return sb.toString();
   }

}
