/*
 * Copyright (C) 2013 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.core;




/** CoapMessageFactory simplifies the creation of CoAP requests and CoAP responses.
  */
public class BasicCoapMessageFactory
{

   /** Creates a new request.
     * @param confirmable whether the request should be CON (true) or NON (false)
     * @param method_code the method code
     * @param payload the response payload, if any
     * @return the (CON or NON) request */
   public static CoapMessage createRequest(boolean confirmable, int method_code, CoapOption[] options, byte[] payload)
   {  try
      {  short type=(confirmable)? CoapMessage.TYPE_CON : CoapMessage.TYPE_NON;
         CoapMessage req=new CoapMessage(type,method_code,CoapMessage.pickMessageId()).setToken(CoapMessage.pickToken()).setOptions(options).setPayload(payload);
         return req;
      }
      catch (CoapMessageFormatException e)
      {  e.printStackTrace();
         return null;
      }
   }   


   /** Creates a new response based on a request.
     * @param req the originated request
     * @param response_code the response code
     * @param payload the response payload, if any
     * @return CON response for CON request or NON response for NON request */
   public static CoapMessage createResponse(CoapMessage req, int response_code, byte[] payload)
   {  try
      {  CoapMessage resp=new CoapMessage(req.getType(),response_code,CoapMessage.pickMessageId()).setToken(req.getToken()).setPayload(payload);
         return resp;
      }
      catch (CoapMessageFormatException e)
      {  e.printStackTrace();
         return null;
      }
   }


   /** Creates a new piggybacked response based on a request.
     * @param req the originated request
     * @param response_code the response code
     * @param payload the response payload, if any
     * @return piggy-backed ACK response for CON request or NON response for NON request */
   public static CoapMessage createPiggyBackedResponse(CoapMessage req, int response_code, byte[] payload)
   {  if (!req.isCON()) return createResponse(req,response_code,payload);
      // else
      try
      {  CoapMessage resp=new CoapMessage(CoapMessage.TYPE_ACK,response_code,req.getMessageId()).setToken(req.getToken()).setPayload(payload);
         return resp;
      }
      catch (CoapMessageFormatException e)
      {  e.printStackTrace();
         return null;
      }
   }
   
}
