/*
 * Copyright (C) 2013 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.core;




/** Listen for CoapProvider events.
  */
public interface CoapProviderListener
{
   /** When a new CoAP message is received.
     * @param coap_provider the CoAP provider
     * @param msg the received CoAP message */
   public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg);

   /** When CoapProvider terminates.
   * @param coap_provider the CoAP provider
   * @param error the error that caused the termination or <i>null</i> */
   //public void onServiceTerminated(CoapProvider coap_provider, Exception error);

}
