package org.zoolu.coap.core;



import org.zoolu.net.SocketAddress;
import org.zoolu.util.Timer;
import org.zoolu.util.TimerListener;
import org.zoolu.util.Random;
import org.zoolu.util.Logger;



/** CoAP reliable transmission.
  */
public class CoapReliableTransmission
{
   /** ACK_TIMEOUT [millisec] */
   public static long ACK_TIMEOUT=2000;
   
   /** ACK_RANDOM_FACTOR */
   public static double ACK_RANDOM_FACTOR=1.5;
  
   /** MAX_RETRANSMIT */
   public static int MAX_RETRANSMIT=4;
  
   /** NSTART */
   public static int NSTART=1;
  
   /** DEFAULT_LEISURE [millisec] */
   public static long DEFAULT_LEISURE=5000;
  
   /** PROBING_RATE [Byte/sec] */
   public static int PROBING_RATE=1;
   
   

   /** CoapReliableTransmission listener */
   CoapReliableTransmissionListener reliable_transmission_listener;

   /** CoapProvider */
   CoapProvider coap_provider;

   /** CoAP message */
   CoapMessage msg;

   /** Remote socket address */
   SocketAddress remote_soaddr;

   /** Retransmission counter */
   int retransmission_counter;

   /** Retransmission timer */
   long retransmission_timout;

   /** Retransmission timer */
   Timer timer;

   /** Retransmission timer */
   TimerListener timer_listener;



   /** Creates a new CoapReliableTransmission. */
   public CoapReliableTransmission(CoapProvider coap_provider, SocketAddress remote_soaddr, CoapReliableTransmissionListener reliable_transmission_listener)
   {  this.coap_provider=coap_provider;
      this.reliable_transmission_listener=reliable_transmission_listener;
      this.remote_soaddr=remote_soaddr;
   }


   /** Sends CoAP message. */
   public void send(CoapMessage msg)
   {  this.msg=msg;
      CoapProviderListener this_cp_listener=new CoapProviderListener()
      {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
         {  processReceivedMessage(coap_provider,msg);
         }
      };
      coap_provider.addListener(new CoapReliableTransmissionId(msg.getMessageId()),this_cp_listener);
      coap_provider.send(msg,remote_soaddr);
      // start retransmission procedure
      timer_listener=new TimerListener()
      {  public void onTimeout(Timer t)
         {  processTimeout(t);
         }
      };
      retransmission_counter=0;
      retransmission_timout=ACK_TIMEOUT+Random.nextInt((int)(ACK_TIMEOUT*(ACK_RANDOM_FACTOR-1)));
      timer=new Timer(retransmission_timout,timer_listener);
      timer.start();
   }


   /** Stops retransmission. */
   public void terminate()
   {  printLog("terminate()");
      if (timer!=null) timer.halt();
      timer=null;
   }


   /** When a new CoAP message is received. */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  printLog("processReceivedMessage("+msg.toString()+")");
      coap_provider.removeListener(new CoapReliableTransmissionId(msg.getMessageId()));
      terminate();
      if (msg.isACK())
      {  if (reliable_transmission_listener!=null) reliable_transmission_listener.onTransmissionAcknowledgement(this,msg);
      }
      else
      if (msg.isRST())
      {  if (reliable_transmission_listener!=null) reliable_transmission_listener.onTransmissionReject(this,msg);
      }
   }
   
   
   /** When the Timer exceeds. */
   private void processTimeout(Timer t)
   {  if (t==timer)
      {  printLog("processTimeout()");
         if (retransmission_counter<MAX_RETRANSMIT)
         {  coap_provider.send(msg,remote_soaddr);
            retransmission_counter++;
            retransmission_timout*=2;
            timer=new Timer(retransmission_timout,timer_listener);
            timer.start();
         }
         else
         {  timer=null;
            if (reliable_transmission_listener!=null) reliable_transmission_listener.onTransmissionTimeout(this);
         }
      }
   }


   /** Prints a log message onto the default log file. */
   private void printLog(String message)
   {  Logger logger=Logger.getDefaultLogger();
      if (logger!=null) logger.append(Logger.LEVEL_DEBUG,getClass(),message);
   }

}
