/*
 * Copyright (C) 2014 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.core;




/** CoapMethodId is used to identify a specific method.
  */
public class CoapMethodId extends CoapId
{
   /** Identifier for ANY messages (regardless their method). */
   //public static final CoapMethodId ANY=new CoapMethodId("ANY"); 
   public static final CoapMethodId ANY=new CoapMethodId(-1); 


   /** Creates a new CoapMethodId.
     * @param method_code the method code */
   public CoapMethodId(int method_code)
   {  //super(getMethodId(CoapMethod.getName(method_code)));
      super(getMethodId(method_code));
   }

   /** Creates a new CoapMethodId.
     * @param method the method name */
   /*public CoapMethodId(String method)
   {  super(getMethodId(method));
   }*/

   /** Creates a new CoapMethodId
     * @param id a method identifier */
   public CoapMethodId(CoapMethodId id)
   {  super(id);
   }

   /** Gets the string value of the method identifier.
     * @param method the method name
     * @return the method identifier */
   /*private static String getMethodId(String method)
   {  return "method-"+method;
   }*/

   /** Gets the string value of the method identifier.
     * @param method_code the method code
     * @return the method identifier */
   private static String getMethodId(int method_code)
   {  if (method_code>=0) return "method-"+method_code;
      else return "method-ANY";
   }
}
