package org.zoolu.coap.core;



import org.zoolu.net.SocketAddress;



/** CoAP transaction client.
  */
public class CoapTransactionClient
{
   /** CoapProvider */
   CoapProvider coap_provider;

   /** Remote socket address */
   SocketAddress remote_soaddr;

   /** CoapTransactionClient listener */
   CoapTransactionClientListener tc_listener;

   /** Request reliable transmission */
   CoapReliableTransmission reliable_transmission=null;
   
   

   /** Creates a new CoapTransactionClient.
    * @param coap_provider the CoAP provider
    * @param remote_soaddr the socket address of the remote CoAP server
    * @param tc_listener the listener of this transaction client */
   public CoapTransactionClient(CoapProvider coap_provider, SocketAddress remote_soaddr, CoapTransactionClientListener tc_listener)
   {  this.coap_provider=coap_provider;
      this.remote_soaddr=remote_soaddr;
      this.tc_listener=tc_listener;
   }


   /** Sends CoAP request.
    * @param req the request message */
   public void request(CoapMessage req)
   {  CoapProviderListener this_cp_listener=new CoapProviderListener()
      {  public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg) {  processReceivedMessage(coap_provider,msg);  }
      };
      coap_provider.addListener(new CoapTransactionId(req.getToken()),this_cp_listener);
      if (req.isCON())
      {  // req=CON,Request
         CoapReliableTransmissionListener this_rt_listener=new CoapReliableTransmissionListener()
         {  public void onTransmissionAcknowledgement(CoapReliableTransmission reliable_transmission, CoapMessage ack) {  processReqestAcknowledgement(reliable_transmission,ack);  }
            public void onTransmissionReject(CoapReliableTransmission reliable_transmission, CoapMessage rst) {  processReqestReject(reliable_transmission,rst);  }
            public void onTransmissionTimeout(CoapReliableTransmission reliable_transmission) {  processReqestTimeout(reliable_transmission);  }
         };
         reliable_transmission=new CoapReliableTransmission(coap_provider,remote_soaddr,this_rt_listener);
         reliable_transmission.send(req);
      }
      else
      {  // req=NON,Request
         coap_provider.send(req,remote_soaddr);
      }
   }


   /** When a new CoAP message is received.
    * @param coap_provider the CoAP provider
    * @param msg the received message */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  if (msg.isResponse())
      {  if (reliable_transmission!=null) reliable_transmission.terminate();
         if (msg.isCON())
         {  // resp=CON,Response
            new CoapReliableReception(coap_provider,msg);
         }
         coap_provider.removeListener(new CoapTransactionId(msg.getToken()));
         if (tc_listener!=null) tc_listener.onTransactionResponse(this,msg);
      }
   }

   
   /** When a new CoAP message is received.
   * @param coap_provider the CoAP provider
   * @param msg the received message */
   /*private void processReceivedResponseRetransmission(CoapProvider coap_provider, CoapMessage msg)
   {  if (msg.isResponse())
      {  if (msg.isCON())
         {  // resp=CON,Response (retransmission)
         }
      }
   }*/

   
   /** When ACK is received confirming the reception of the request.
   * @param reliable_transmission the reliable transmission
   * @param ack the received message */
   private void processReqestAcknowledgement(CoapReliableTransmission reliable_transmission, CoapMessage ack)
   {  if (ack.isResponse())
      {  // piggybacked response
         processReceivedMessage(coap_provider,ack);
      }
   }


   /** When RST is received confirming the reception of the request.
   * @param reliable_transmission the reliable transmission
   * @param rst the received message */
   private void processReqestReject(CoapReliableTransmission reliable_transmission, CoapMessage rst)
   {  if (tc_listener!=null) tc_listener.onTransactionFailure(this);
   }


   /** When maximum request retransmission has been reached without receiving any ACK (or RST).
   * @param reliable_transmission the reliable transmission */
   private void processReqestTimeout(CoapReliableTransmission reliable_transmission)
   {  if (tc_listener!=null) tc_listener.onTransactionFailure(this);
   }
   
}
