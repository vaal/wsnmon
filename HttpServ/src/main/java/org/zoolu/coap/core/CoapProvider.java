/*
 * Copyright (C) 2013 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.core;



import org.zoolu.net.*;
import org.zoolu.util.Random;
import org.zoolu.util.Logger;
import org.zoolu.util.PacketLogger;
import java.util.Hashtable;
import java.util.Enumeration;



/** CoAP message communication service for sending and receiving CoAP messages.
  */
public class CoapProvider
{
   /** CoAP UDP port number ANY */
   public static final int ANY_PORT=0;
   
   /** Default CoAP UDP port number */
   public static final int DEFAUL_UDP_PORT=5683;
   
   /** Packet error rate introduced by this node when sending CoAP messages. This is just for testing. */
   public static double PACKET_ERROR_RATE=0.0;


   /** Method listeners (Hashtable<CoapId,CoapProviderListener>) */
   Hashtable<CoapId,CoapProviderListener> listeners=new Hashtable<CoapId,CoapProviderListener>();


   /** UDP provider */
   UdpProvider udp_provider;



   /** Creates a new CoapProvider. */
  public CoapProvider() throws java.net.SocketException
  {  init(new UdpSocket(DEFAUL_UDP_PORT));
  }

  
   /** Creates a new CoapProvider.
     * @param port the local UDP port number */
   public CoapProvider(int port) throws java.net.SocketException
   {  init(new UdpSocket(port));
   }

   
   /** Creates a new CoapProvider.
    * @param udp_socket the UDP socket */
   public CoapProvider(UdpSocket udp_socket)
   {  init(udp_socket);
   }

  
   /** Initializes the CoapProvider.
    * @param udp_socket the UDP socket */
   private void init(UdpSocket udp_socket)
   {  UdpProviderListener udp_provider_listener=new UdpProviderListener()
      {  public void onReceivedPacket(UdpProvider udp, UdpPacket packet)
         {  processReceivedPacket(udp,packet);
         }
         public void onServiceTerminated(UdpProvider udp, Exception error)
         {  processServiceTerminated(udp,error);
         }   
      };
      udp_provider=new UdpProvider(udp_socket,udp_provider_listener);
   }

  
   /** Adds a new CoapProvider listener.
     * @param id the identifier of a CoAP method, a reliable transmission, or a transaction. It specifies the kind of messages that the listener is interested to receive
     * @param listener the CoapProvider listener */
   public void addListener(CoapId id, CoapProviderListener listener)
   {  listeners.put(id,listener);
   }


   /** Removes a CoapProvider listener.
     * @param id the identifier of a CoAP method, a reliable transmission, or a transaction, associated to the listener that has to be removed */
   public void removeListener(CoapId id)
   {  listeners.remove(id);
   }


   /** Removes a CoapProvider listener.
     * @param listener the listener that has to be removed */
   public void removeListener(CoapProviderListener listener)
   {  for (Enumeration<CoapId> i=listeners.keys(); i.hasMoreElements(); )
      {  Object key=i.nextElement();
         if (listeners.get(key)==listener)
         {  listeners.remove(key);
            return;
         }
      }
   }


   /** Sends a new CoAP message.
     * @param msg the CoAP message */
   /*public void send(CoapMessage msg)
   {  printLog("send("+msg.toString()+")");
      SocketAddress remote_soaddr=msg.getRemoteSoAddress();
      if (remote_soaddr!=null) send(msg,remote_soaddr);
      else
      {  // try to use request URI
         // TODO..
         printLog("send(): no destination address found");
      }
   }*/


   /** Sends a new CoAP message.
     * @param msg the CoAP message
     * @param remote_soaddr the remote socket address where the message has to be sent to */
   public void send(CoapMessage msg, SocketAddress remote_soaddr)
   {  printLog("send("+msg.toString()+","+remote_soaddr+")");
      if (PACKET_ERROR_RATE>0 && Random.nextDouble()<PACKET_ERROR_RATE)
      {  printLog("send(): packet dropped");
         return;
      }
      // else
      try
      {  byte[] data=msg.getBytes();
         IpAddress remote_ipaddr=remote_soaddr.getAddress();
         int remote_port=remote_soaddr.getPort();
         if (remote_port<=0) remote_port=DEFAUL_UDP_PORT;
         udp_provider.send(new UdpPacket(data,remote_ipaddr,remote_port));
         printPacketLog(udp_provider.getUdpSocket().getLocalAddress().toString()+":"+udp_provider.getUdpSocket().getLocalPort(),remote_soaddr.toString(),msg.toString(),data,0,data.length);
      }
      catch (java.io.IOException e)
      {  e.printStackTrace();
      }
   }


   /** Stops the CoAP provider. */
   public void halt()
   {  UdpSocket udp_socket=udp_provider.getUdpSocket();
      udp_provider.halt();
      try {  Thread.sleep(2000);  } catch (Exception e) {}
      udp_socket.close();
   }


   /** When a new UDP datagram is received.
     * @param udp the UDP provider
     * @param packet the received UDP datagram */
   private void processReceivedPacket(UdpProvider udp, UdpPacket packet)
   {  try
      {  CoapMessage msg=new CoapMessage(packet.getData(),packet.getOffset(),packet.getLength());
         printLog("processReceivedPacket("+msg.toString()+")");
         msg.setRemoteSoAddress(new SocketAddress(packet.getIpAddress(),packet.getPort()));
         printPacketLog(msg.getRemoteSoAddress().toString(),udp_provider.getUdpSocket().getLocalAddress().toString()+":"+udp_provider.getUdpSocket().getLocalPort(),msg.toString(),packet.getData(),packet.getOffset(),packet.getLength());
         /*if (PACKET_ERROR_RATE>0 && Random.nextDouble()<PACKET_ERROR_RATE)
         {  printLog("processReceivedPacket(): packet dropped");
            return;
         }
         // else
         */
         CoapId id=new CoapReliableTransmissionId(msg.getMessageId());
         if (listeners.containsKey(id)) ((CoapProviderListener)listeners.get(id)).onReceivedMessage(this,msg);
         else
         {  byte[] token=msg.getToken();
            id=(token!=null)? new CoapTransactionId(token) : null;
            if (id!=null && listeners.containsKey(id)) ((CoapProviderListener)listeners.get(id)).onReceivedMessage(this,msg);
            else
            {  id=(msg.isRequest())? new CoapTransferId(msg.getCode(),msg.getRemoteSoAddress()) : null;
               if (id!=null && listeners.containsKey(id)) ((CoapProviderListener)listeners.get(id)).onReceivedMessage(this,msg);
               else
               {  id=(msg.isRequest())? new CoapMethodId(msg.getCode()) : null;
                  if (id!=null && listeners.containsKey(id)) ((CoapProviderListener)listeners.get(id)).onReceivedMessage(this,msg);
                  else
                  {  if (listeners.containsKey(CoapMethodId.ANY)) ((CoapProviderListener)listeners.get(CoapMethodId.ANY)).onReceivedMessage(this,msg);
                     else
                     printLog("processReceivedPacket(): no listener found");
                  }
               }
            }      
         }
      }
      catch (CoapMessageFormatException e)
      {  e.printStackTrace();
      }
   }


   /** When UdpProvider terminates.
     * @param udp the UDP provider
     * @param error the error that caused the termination, or <i>null</i> */
   private void processServiceTerminated(UdpProvider udp, Exception error) 
   {  //if (listener!=null) listener.onServiceTerminated(this,error);
      // do something
   }  


   /** Prints a log message onto the default log file.
     * @param the message to be printed */
   private void printLog(String str)
   {  Logger logger=Logger.getDefaultLogger();
      if (logger!=null) logger.append(Logger.LEVEL_MEDIUM,getClass(),str);
   }


   /** Prints a packet onto the default trace file.
    * @param src_address the source address of the packet
    * @param dest_address the destination address of the packet
    * @param description a packet description
    * @param buf the buffer containing the packet
    * @param off the offset within the packet
    * @param len the packet length */
   private void printPacketLog(String src_address, String dest_address, String description, byte[] buf, int off, int len)
   {  PacketLogger logger=PacketLogger.getDefaultPacketLogger();
      if (logger!=null) logger.append(src_address,dest_address,description,buf,off,len);
   }

}
