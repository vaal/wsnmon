package org.zoolu.coap.core;



import org.zoolu.util.Timer;
import org.zoolu.util.TimerListener;
import org.zoolu.util.Logger;



/** CoAP transaction server.
  */
public class CoapTransactionServer
{
   /** waiting time for piggybacked responses (in milliseconds) */
   public static long PIGGYBACKED_TIME=200;

   /** CoapProvider */
   CoapProvider coap_provider;

   /** CoapTransactionServer listener */
   CoapTransactionServerListener ts_listener;

   /** Remote socket address */
   //SocketAddress remote_soaddr;

   /** Whether request is confirmable */
   //boolean confirmable;

   /** Request */
   CoapMessage req;

   /** Response */
   CoapMessage resp=null;

   /** Piggybacked timer */
   Timer piggybacked_timer=null;



   /** Creates a new CoapTransactionServer.
     * @param coap_provider the CoAP provider
     * @param req the request message
     * @param ts_listener transaction server listener */
   public CoapTransactionServer(CoapProvider coap_provider, CoapMessage req, CoapTransactionServerListener ts_listener)
   {  this.coap_provider=coap_provider;
      this.req=req;
      this.ts_listener=ts_listener;
      if (req.isCON())
      {  if (PIGGYBACKED_TIME>0)
         {  TimerListener t_listener=new TimerListener() {  public void onTimeout(Timer t) {  processPiggyBackedTimeout();  }  };
            piggybacked_timer=new Timer(PIGGYBACKED_TIME,t_listener);
            piggybacked_timer.start();
         }
         else new CoapReliableReception(coap_provider,req);
      }
   }


   /** When piggybacked timeout expires. */
   private void processPiggyBackedTimeout()
   {  printLog("processPiggyBackedTiemout()");
      // send ACK
      new CoapReliableReception(coap_provider,req);
   }


   /** Sends CoAP response.
     * @param response_code the response code
     * @param payload the payload, if any, or null */
   /*public synchronized void respond(int response_code, byte[] payload)
   {  if (resp!=null) return;
      // else
      CoapMessage resp=CoapMessageFactory.createResponse(req,response_code,payload);
      respond(resp);
   }*/


   /** Sends CoAP response.
     * @param resp the response message */
   public synchronized void respond(CoapMessage resp)
   {  if (this.resp!=null) return;
      // else
      this.resp=resp;
      resp.setToken(req.getToken());
      // try to send piggybacked response
      if (PIGGYBACKED_TIME>0 && piggybacked_timer.isRunning())
      {  // the request is CON and is still waiting for a response: send the response as an ACK (or RST) (piggybacked)
         piggybacked_timer.halt();
         if (!resp.isRST()) resp.setACK();
         resp.setMessageId(req.getMessageId());
         new CoapReliableReception(coap_provider,req,resp);
      }
      else
      // if you want to convert possible ACK responses to CON responses, do it here
      //..
      if (resp.isCON() || resp.isNON())
      {  // if you want to have always CON responses for CON requests, do it here
         //..
         if (resp.isCON())
         {  // resp=CON,Response
            CoapReliableTransmissionListener rt_listener=new CoapReliableTransmissionListener()
            {  public void onTransmissionAcknowledgement(CoapReliableTransmission reliable_transmission, CoapMessage ack)
               {  processTransmissionAcknowledgement(reliable_transmission,ack);
               }
               public void onTransmissionReject(CoapReliableTransmission reliable_transmission, CoapMessage rst)
               {  processTransmissionReject(reliable_transmission,rst);
               }
               public void onTransmissionTimeout(CoapReliableTransmission reliable_transmission)
               {  processTransmissionTimeout(reliable_transmission);
               }
            };
            new CoapReliableTransmission(coap_provider,req.getRemoteSoAddress(),rt_listener).send(resp);
         }
         else
         if (resp.isNON())
         {  // resp=NON,Response
            coap_provider.send(resp,req.getRemoteSoAddress());
         }
         else
         { // what doing with ACK or RST responses here?
           // TODO
         }
      }
   }


   /** When a new CoAP message is received. */
   /*private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {
   }*/


   /** When ACK is received confirming a reliable transmission. */
   private void processTransmissionAcknowledgement(CoapReliableTransmission reliable_transmission, CoapMessage ack)
   {  // do something?
   }


   /** When RST is received confirming a reliable transmission. */
   private void processTransmissionReject(CoapReliableTransmission reliable_transmission, CoapMessage rst)
   {  if (ts_listener!=null) ts_listener.onTransactionFailure(this);
   }


   /** When maximum retransmission has been reached without receiving any ACK (or RST). */
   private void processTransmissionTimeout(CoapReliableTransmission reliable_transmission)
   {  if (ts_listener!=null) ts_listener.onTransactionFailure(this);
   }


   /** Prints a log message. */
   public void printLog(String str)
   {  Logger logger=Logger.getDefaultLogger();
      if (logger!=null) logger.append(Logger.LEVEL_MEDIUM,getClass(),str);
   }
   
}
