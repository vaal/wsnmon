/*
 * Copyright (C) 2014 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */



package org.zoolu.coap.core;

import org.zoolu.net.SocketAddress;




/** CoapTransferId is used to identify a given method with a specific remote end-point.
  */
public class CoapTransferId extends CoapId
{
   /** Creates a new CoapTransferId.
     * @param method_code the method code
   * @param remote_soaddr the socket address of the remote end-point */
   public CoapTransferId(int method_code, SocketAddress remote_soaddr)
   {  super(getTransferId(method_code,remote_soaddr));
   }

   /** Creates a new CoapTransferId
     * @param id a transfer identifier */
   public CoapTransferId(CoapTransferId id)
   {  super(id);
   }

  /** Gets the string value of the method identifier.
     * @param method_code the method code
     * @param remote_soaddr the socket address of the remote end-point
     * @return the transfer identifier */
   private static String getTransferId(int method_code, SocketAddress remote_soaddr)
   {  String host=remote_soaddr.getAddress().toString();
      int port=remote_soaddr.getPort();
      if (port<=0) port=CoapProvider.DEFAUL_UDP_PORT;
      return "transfer-"+method_code+"-"+host+":"+port;
   }
}
