package org.zoolu.coap.core;



import org.zoolu.util.Timer;
import org.zoolu.util.TimerListener;



/** CoAP reliable reception.
  */
public class CoapReliableReception
{

   /** EXCHANGE_LIFETIME [millisec] */
   //public static long EXCHANGE_LIFETIME=(long)(CoapReliableTransmission.ACK_TIMEOUT*((1<<CoapReliableTransmission.MAX_RETRANSMIT)-1)*CoapReliableTransmission.ACK_RANDOM_FACTOR);
   public static long EXCHANGE_LIFETIME=100000;
   
   /** NON_LIFETIME [millisec] */
   public static long NON_LIFETIME=100000;



   /** CoapProvider */
   CoapProvider coap_provider;

   /** Message ID */
   int message_id;
   
   /** ACK message */
   CoapMessage ack=null;

   

   /** Creates a new CoapReliableReception.
    * @param coap_provider the CoAP provider
    * @param con the confirmable message (that is the message that has to be confirmed) */
   public CoapReliableReception(CoapProvider coap_provider, CoapMessage con)
   {  init(coap_provider,con,null);
   }


   /** Creates a new CoapReliableReception.
    * @param coap_provider the CoAP provider
    * @param con the confirmable message (that is the message that has to be confirmed)
    * @param ack the confirmation message (to be sent) */
   public CoapReliableReception(CoapProvider coap_provider, CoapMessage con, CoapMessage ack)
   {  init(coap_provider,con,ack);
   }


   /** Inits the CoapReliableReception.
   * @param coap_provider the CoAP provider
   * @param con the confirmable message (that is the message that has to be confirmed)
   * @param ack the confirmation message (to be sent), or null */
   private void init(CoapProvider coap_provider, CoapMessage con, CoapMessage ack)
   {  this.coap_provider=coap_provider;
      message_id=con.getMessageId();
      CoapProviderListener this_cp_listener=new CoapProviderListener()
      {  @Override
         public void onReceivedMessage(CoapProvider coap_provider, CoapMessage msg) {  processReceivedMessage(coap_provider,msg);  }
      };
      coap_provider.addListener(new CoapReliableTransmissionId(message_id),this_cp_listener);
      if (ack==null)
      try
      {  ack=new CoapMessage(CoapMessage.TYPE_ACK,CoapMessage.EMPTY,message_id);
      }
      catch (CoapMessageFormatException e)
      {  e.printStackTrace();
      }
      this.ack=ack;
      if (ack!=null) coap_provider.send(ack,con.getRemoteSoAddress());
      TimerListener timer_listener=new TimerListener()
      {  @Override
         public void onTimeout(Timer t) {  processTimeout(t);  }
      };
      Timer timer=new Timer(EXCHANGE_LIFETIME,timer_listener);
      timer.start();
   }


   /** When a new CoAP message is received.
    * @param coap_provider the CoAP provider
    * @param msg the received CoAP message */
   private void processReceivedMessage(CoapProvider coap_provider, CoapMessage msg)
   {  if (ack!=null) coap_provider.send(ack,msg.getRemoteSoAddress());
   }


   /** When the Timer exceeds.
    * @param t the timer */
   private void processTimeout(Timer t)
   {  coap_provider.removeListener(new CoapReliableTransmissionId(message_id));
      coap_provider=null;
   }

}
