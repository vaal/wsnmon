/*
 * Copyright (C) 2014 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.core;




/** CoapReliableTransmissionId is used to identify a specific reliable transmission.
  */
public class CoapReliableTransmissionId extends CoapId
{
   /** Creates a new CoapReliableTransmissionId.
     * @param message_id the message-id */
   public CoapReliableTransmissionId(int message_id)
   {  super("messageid-"+message_id);
   }

   /** Creates a new CoapReliableTransmissionId.
     * @param id a transaction identifier */
   public CoapReliableTransmissionId(CoapReliableTransmissionId id)
   {  super(id);
   }

   /** Gets a reliable transmission identifier.
     * @param message_id the message-id
     * @return the string value for a reliable transmission identifier */
   /*private static String getReliableTransmissionId(int message_id)
   {  return "messageid-"+message_id;
   }*/
}
