/*
 * Copyright (C) 2014 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.core;



import org.zoolu.util.ByteUtils;



/** CoapTransactionId is used to identify a specific transaction.
  */
public class CoapTransactionId extends CoapId
{
   /** Creates a new CoapTransactionId.
     * @param token the message token */
   public CoapTransactionId(byte[] token)
   {  super("token-"+ByteUtils.asHex(token));
   }

   /** Creates a new CoapTransactionId.
     * @param id a transaction identifier */
   public CoapTransactionId(CoapTransactionId id)
   {  super(id);
   }

   /** Gets a transaction identifier.
     * @param token the message token
     * @return the string value for a transaction identifier */
   /*private static String getTransactionId(byte[] token)
   {  return "token-"+BasicCoapMessage.asHex(token);
   }*/
}
