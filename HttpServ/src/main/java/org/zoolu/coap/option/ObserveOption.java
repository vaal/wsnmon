package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Observe option (see RFC 7252).
 */
public class ObserveOption extends CoapOption
{
   /** Register */
   public static final int REGISTER=0;

   
   /** Unregister */
   public static final int UNREGISTER=1;

   

   /** Creates a new Observe option.
    * @param co CoapOption to be copied */
   public ObserveOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new Observe option.
    * @param seq_num sequence number */
   public ObserveOption(int seq_num)
   {  super(CoapOptionNumber.Observe,seq_num);
   }


   /** Gets the sequence number.
    * @return the sequence number */
   public int getSequenceNumber()
   {  return (int)getValueAsUnit();
   }


   /** Whether it is a registration.
    * @return <i>true</i> if it is registration */
   public boolean isRegister()
   {  return getSequenceNumber()==REGISTER;
   }

}

