package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Proxy-Uri option (see RFC 7252).
 */
public class ProxyUriOption extends CoapOption
{

   /** Creates a new Proxy-Uri option.
    * @param co CoapOption to be copied */
   public ProxyUriOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new Proxy-Uri option.
    * @param uri the URI */
   public ProxyUriOption(String uri)
   {  super(CoapOptionNumber.ProxyUri,uri);
   }


   /** Gets the URI.
    * @return the URI */
   public String getURI()
   {  return getValueAsString();
   }

}
