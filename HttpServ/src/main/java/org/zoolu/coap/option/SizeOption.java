package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Size-type option (see RFC 7252).
 */
abstract class SizeOption extends CoapOption
{

   /** Creates a new size option.
    * @param co CoapOption to be copied */
   public SizeOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new size option.
    * @param opt_number option number
    * @param size size of the resource representation in a request */
   public SizeOption(int opt_number, int size)
   {  super(opt_number,size);
   }


   /** Gets the size of the resource representation in a request.
    * @return the size */
   public int getSize()
   {  return (int)getValueAsUnit();
   }

}
