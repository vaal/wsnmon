package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Block1 options (see <draft-ietf-core-block>).
 */
public class Block1Option extends BlockOption
{

   /** Creates a new Block1 option.
    * @param co CoapOption to be copied */
   public Block1Option(CoapOption co)
   {  super(co);
   }

   /** Creates a new Block1 option.
    * @param size_exp the 2-power exponent of the size of the block (equals to SZX + 4)
    * @param more_blocks whether more blocks are following (M)
    * @param block_num the relative number of the block (NUM) within a sequence of blocks with the given size */
   public Block1Option(int size_exp, boolean more_blocks, long block_num)
   {  super(CoapOptionNumber.Block1,size_exp,more_blocks,block_num);
   }

}
