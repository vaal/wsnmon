package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Proxy-Scheme option (see RFC 7252).
 */
public class ProxySchemeOption extends CoapOption
{

   /** Creates a new Proxy-Scheme option.
    * @param co CoapOption to be copied */
   public ProxySchemeOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new Proxy-Scheme option.
    * @param scheme the scheme */
   public ProxySchemeOption(String scheme)
   {  super(CoapOptionNumber.ProxyScheme,scheme);
   }


   /** Gets the scheme.
    * @return the scheme */
   public String getScheme()
   {  return getValueAsString();
   }

}
