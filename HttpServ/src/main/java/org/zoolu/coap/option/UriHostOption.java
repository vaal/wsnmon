package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Uri-Host option (see RFC 7252).
 */
public class UriHostOption extends CoapOption
{

   /** Creates a new Uri-Host option.
    * @param co CoapOption to be copied */
   public UriHostOption(CoapOption co)
   {  super(co);
   }

   
   /** Creates a new Uri-Host option.
    * @param host the URI host */
   public UriHostOption(String host)
   {  super(CoapOptionNumber.UriHost,host);
   }


   /** Gets the URI host.
    * @return the host */
   public String getHost()
   {  return getValueAsString();
   }

}
