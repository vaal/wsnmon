package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Location-Query option (see RFC 7252).
 */
public class LocationQueryOption extends CoapOption
{

   /** Creates a new Location-Query option.
    * @param co CoapOption to be copied */
   public LocationQueryOption(CoapOption co)
   {  super(co);
   }

   
   /** Creates a new Location-Query option.
    * @param query the location query */
   public LocationQueryOption(String query)
   {  super(CoapOptionNumber.LocationQuery,query);
   }


   /** Gets the location query.
    * @return the query */
   public String getQuery()
   {  return getValueAsString();
   }

}
