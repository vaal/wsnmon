package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Location-Path option (see RFC 7252).
 */
public class LocationPathOption extends CoapOption
{

   /** Creates a new Location-Path option.
    * @param co CoapOption to be copied */
   public LocationPathOption(CoapOption co)
   {  super(co);
   }

   
   /** Creates a new Location-Path option.
    * @param path the location path */
   public LocationPathOption(String path)
   {  super(CoapOptionNumber.LocationPath,path);
   }


   /** Gets the location path.
    * @return the path */
   public String getPath()
   {  return getValueAsString();
   }

}
