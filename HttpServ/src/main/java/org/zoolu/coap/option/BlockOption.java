package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;


/** CoAP Block-type option (see: <draft-ietf-core-block> "Blockwise transfers in CoAP").
 */
abstract class BlockOption extends CoapOption
{

   /** Creates a new abstract Block option.
    * @param co CoapOption to be copied */
   protected BlockOption(CoapOption co)
   {  super(co);
   }

   /** Creates a new abstract Block option.
    * @param opt_number the option number
    * @param szx the 2-power exponent of the size of the block, minus 4 (SZX)
    * @param more_blocks whether more blocks are following (M)
    * @param block_num the relative number of the block (NUM) within a sequence of blocks with the given size */
   protected BlockOption(int opt_number, short szx, boolean more_blocks, long block_num)
   {  super(opt_number,(block_num<<4)|(more_blocks?0x8:0x0)|(szx&0x7));
   }


   /** Creates a new abstract Block option.
    * @param opt_number the option number
    * @param size the size of the block (it is equal to 2**(SZX + 4))
    * @param more_blocks whether more blocks are following (M)
    * @param block_num the relative number of the block (NUM) within a sequence of blocks with the given size */
   protected BlockOption(int opt_number, int size, boolean more_blocks, long block_num)
   {  super(opt_number,(block_num<<4)|(more_blocks?0x8:0x0)|sizeToSzx(size));
   }


   /** Gets SZX, that is the 2-power exponent of the size of the block minus 4.
    * @return the SZX value */
   public int getSZX()
   {  return (int)getValueAsUnit()&0x7;
   }


   /** Gets the size of the block.
    * @return the size */
   public int getSize()
   {  return szxToSize((short)(getValueAsUnit()&0x7));
   }


   /** Whether there are more blocks (flag M).
    * @return <i>true</i> if there are more blocks (that is if flag M equals to 1) */
   public boolean moreBlocks()
   {  return (getValueAsUnit()&0x8)==8;
   }


   /** Gets the block sequence number.
    * @return the block sequence number */
   public long getSequenceNumber()
   {  return getValueAsUnit()>>4;
   }

   
   /** SZX to size.
    * @param szx the szx value
    * @return the size */
   private static int szxToSize(short szx)
   {  return 16<<(szx&0x7);
   }


   /** Size to SZX.
   * @param size the size
   * @return SZX */
   private static short sizeToSzx(int size)
   {  if (size<=16) return 0;
      // else
      if (size<=32) return 1;
      // else
      if (size<=64) return 2;
      // else
      if (size<=128) return 3;
      // else
      if (size<=256) return 4;
      // else
      if (size<=512) return 5;
      // else
      if (size<=1024) return 6;
      // else
      throw new RuntimeException("sizeToSzx(): size ("+size+") too big for SZX");
   }

}
