package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Size1 option (see RFC 7252).
 */
public class Size1Option extends SizeOption
{

   /** Creates a new Size1 option.
    * @param co CoapOption to be copied */
   public Size1Option(CoapOption co)
   {  super(co);
   }


   /** Creates a new Size1 option.
    * @param size size of the resource representation in a request */
   public Size1Option(int size)
   {  super(CoapOptionNumber.Size1,size);
   }

}
