package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Size2 option (see <draft-ietf-core-block>).
 */
public class Size2Option extends SizeOption
{

   /** Creates a new Size2 option.
    * @param co CoapOption to be copied */
   public Size2Option(CoapOption co)
   {  super(co);
   }


   /** Creates a new Size2 option.
    * @param size size of the resource representation in a request */
   public Size2Option(int size)
   {  super(CoapOptionNumber.Size2,size);
   }

}
