package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Accept option (see RFC 7252).
 */
public class AcceptOption extends CoapOption
{

   /** Creates a new Accept option.
    * @param co CoapOption to be copied */
   public AcceptOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new Accept option.
    * @param contet_format identifier of the acceptable content format */
   public AcceptOption(int contet_format)
   {  super(CoapOptionNumber.Accept,contet_format);
   }


   /** Gets the identifier of the acceptable content format.
    * @return the content format identifier */
   public int getContentFormatIdentifier()
   {  return (int)getValueAsUnit();
   }

}
