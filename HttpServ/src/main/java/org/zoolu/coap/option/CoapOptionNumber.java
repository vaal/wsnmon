/*
 * Copyright (C) 2013 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.coap.option;




/** Collection of CoAP option numbers.
  */
public class CoapOptionNumber
{
   // Basic options (RFC 7252):
   
   /** Option Reserved */
   public static final int reserved=0;

   /** Option If-Match */
   public static final int IfMatch=1;

   /** Option Uri-Host */
   public static final int UriHost=3;

   /** Option ETag */
   public static final int ETag=4;

   /** Option If-None-Match */
   public static final int IfNoneMatch=5;

   /** Option Uri-Port */
   public static final int UriPort=7;

   /** Option Location-Path */
   public static final int LocationPath=8;

   /** Option Uri-Path */
   public static final int UriPath=11;

   /** Option Content-Format */
   public static final int ContentFormat=12;

   /** Option Max-Age */
   public static final int MaxAge=14;

   /** Option Uri-Query */
   public static final int UriQuery=15;

   /** Option Accept */
   public static final int Accept=16;

   /** Option Location-Query */
   public static final int LocationQuery=20;

   /** Option Proxy-Uri */
   public static final int ProxyUri=35;

   /** Option Proxy-Scheme */
   public static final int ProxyScheme=39;

   /** Option Size1 */
   public static final int Size1=60;

   // Blockwise transfer options (draft-ietf-core-block-15):

   /** Block2 */
   public static final int Block2=23;

   /** Block1 */
   public static final int Block1=27;
   
   /** Size2 */
   public static final int Size2=28;

   // Observing Resources options (draft-ietf-core-observe-14):

   /** Observe */ 
   public static final int Observe=6;

   
   
   /** Gets option name. */
   public static String getOptionName(int number)
   {  switch (number)
      {  // RFC 7252:
         case  reserved : return "reserved";
         case  IfMatch : return "If-Match";
         case  UriHost : return "Uri-Host";
         case  ETag : return "ETag";
         case  IfNoneMatch : return "If-None-Match";
         case  UriPort : return "Uri-Port";
         case  LocationPath : return "Location-Path";
         case  UriPath : return "Uri-Path";
         case  ContentFormat : return "Content-Format";
         case  MaxAge : return "Max-Age";
         case  UriQuery : return "Uri-Query";
         case  Accept : return "Accept";
         case  LocationQuery : return "Location-Query";
         case  ProxyUri : return "Proxy-Uri";
         case  ProxyScheme : return "Proxy-Scheme";
         case  Size1 : return "Size1";
         
         // draft-ietf-core-block-15:
         case  Block2 : return "Block2";
         case  Block1 : return "Block1";
         case  Size2 : return "Size2";
         
         // draft-ietf-core-observe-14:
         case  Observe : return "Observe";
      }
      // else
      return "unknown";
   } 


   /** Whether it is a Critical option number.
     * @return true if Critical option number */
   public static boolean isCritical(int number)
   {  return (number&0x1)==0x1;
   }


   /** Whether it is a UnSafe option number.
     * @return true if UnSafe option number */
   public static boolean isUnSafe(int number)
   {  return (number&0x2)==0x2;
   }


   /** Whether it is a NoCacheKey option number.
     * @return true if NoCacheKey option number */
   public static boolean isNoCacheKey(int number)
   {  return (number&0x1e)==0x1c;
   }

}