package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP If-Match option (see RFC 7252).
 */
public class IfMatchOption extends CoapOption
{
  
   /** Creates a new If-Match option.
    * @param co CoapOption to be copied */
   public IfMatchOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new If-Match option. */
   public IfMatchOption()
   {  super(CoapOptionNumber.IfMatch,EMPTY);
   }


   /** Creates a new If-Match option.
    * @param entity_tag an entity-tag */
   public IfMatchOption(String entity_tag)
   {  super(CoapOptionNumber.IfMatch,entity_tag);
   }


   /** Gets the If-Match value.
    * @return the an empty string or an entity-tag */
   public byte[] getIfMatchValue()
   {  return getValueAsOpaque();
   }

}
