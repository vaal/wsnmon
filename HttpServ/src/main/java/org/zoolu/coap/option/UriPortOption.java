package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Uri-Port option (see RFC 7252).
 */
public class UriPortOption extends CoapOption
{

   /** Creates a new Uri-Port option.
    * @param co CoapOption to be copied */
   public UriPortOption(CoapOption co)
   {  super(co);
   }

   
   /** Creates a new Uri-Port option.
    * @param port the URI port */
   public UriPortOption(int port)
   {  super(CoapOptionNumber.UriPort,port);
   }


   /** Gets the URI port.
    * @return the port */
   public int getPort()
   {  return (int)getValueAsUnit();
   }

}
