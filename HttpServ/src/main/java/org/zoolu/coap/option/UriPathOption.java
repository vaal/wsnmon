package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Uri-Path option (see RFC 7252).
 */
public class UriPathOption extends CoapOption
{

   /** Creates a new Uri-Path option.
    * @param co CoapOption to be copied */
   public UriPathOption(CoapOption co)
   {  super(co);
   }

   
   /** Creates a new Uri-Path option.
    * @param path the URI path */
   public UriPathOption(String path)
   {  super(CoapOptionNumber.UriPath,path);
   }


   /** Gets the URI path.
    * @return the path */
   public String getPath()
   {  return getValueAsString();
   }

}
