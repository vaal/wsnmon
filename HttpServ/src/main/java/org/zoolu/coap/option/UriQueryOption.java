package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Uri-Query option (see RFC 7252).
 */
public class UriQueryOption extends CoapOption
{

   /** Creates a new Uri-Query option.
    * @param co CoapOption to be copied */
   public UriQueryOption(CoapOption co)
   {  super(co);
   }

   
   /** Creates a new Uri-Query option.
    * @param query the URI query */
   public UriQueryOption(String query)
   {  super(CoapOptionNumber.UriQuery,query);
   }


   /** Gets the URI query.
    * @return the query */
   public String getQuery()
   {  return getValueAsString();
   }

}
