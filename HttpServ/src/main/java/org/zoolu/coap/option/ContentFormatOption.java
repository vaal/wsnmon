package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Content-Format option (see RFC 7252).
 */
public class ContentFormatOption extends CoapOption
{
   /** Format text/plain;charset=utf-8 */
   public static final int FORMAT_TEXT_PLAIN_UTF8=0;
   
   /** Format application/link-format */
   public static final int FORMAT_APP_LINK_FORMAT=40;
   
   /** Format application/xml */
   public static final int FORMAT_APP_XML=41;

   /** Format application/octet-stream */
   public static final int FORMAT_APP_OCTECT_STREAM=42;

   /** Format application/exi */
   public static final int FORMAT_APP_EXI=47;

   /** Format application/json */
   public static final int FORMAT_APP_JSON=50;

   
   /** Gets a string representation of a content-format.
    * @param contet_format content format identifier
    * @return the string representation of the content-format */
   public static String getContentFormat(int contet_format)
   {  switch (contet_format)
      {  case FORMAT_TEXT_PLAIN_UTF8 : return "text/plain;charset=utf-8";
         case FORMAT_APP_LINK_FORMAT : return "application/link-format";
         case FORMAT_APP_XML : return "application/xml";
         case FORMAT_APP_OCTECT_STREAM : return "application/octet-stream";
         case FORMAT_APP_EXI : return "application/exi";
         case FORMAT_APP_JSON : return "application/json";
      }
      return null;
   }
   
   
   /** Gets a content-format identifier.
    * @param contet_format the string representation of the content-format
    * @return content format identifier */
   public static int getContentFormatIdentifier(String contet_format)
   {  if (contet_format.equals("text/plain;charset=utf-8")) return FORMAT_TEXT_PLAIN_UTF8;
      else
      if (contet_format.equals("application/link-format")) return FORMAT_APP_LINK_FORMAT;
      else
      if (contet_format.equals("application/xml")) return FORMAT_APP_XML;
      else
      if (contet_format.equals("application/octet-stream")) return FORMAT_APP_OCTECT_STREAM;
      else
      if (contet_format.equals("application/exi")) return FORMAT_APP_EXI;
      else
      if (contet_format.equals("application/json")) return FORMAT_APP_JSON;
      // else
      return -1;
   }
   
   
   /** Creates a new Content-Format option.
    * @param co CoapOption to be copied */
   public ContentFormatOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new Content-Format option.
    * @param contet_format content format identifier */
   public ContentFormatOption(int contet_format)
   {  super(CoapOptionNumber.ContentFormat,contet_format);
   }


   /** Gets the content format identifier.
    * @return the content format identifier */
   public int getContentFormatIdentifier()
   {  return (int)getValueAsUnit();
   }

}
