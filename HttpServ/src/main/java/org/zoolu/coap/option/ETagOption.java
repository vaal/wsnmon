package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP ETag option (see RFC 7252).
 */
public class ETagOption extends CoapOption
{

   /** Creates a new ETag option.
    * @param co CoapOption to be copied */
   public ETagOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new ETag option.
    * @param entity_tag the entity-tag for the "tagged representation" */
   public ETagOption(byte[] entity_tag)
   {  super(CoapOptionNumber.ETag,entity_tag);
   }


   /** Gets the entity-tag.
    * @return the entity-tag */
   public byte[] getEntityTag()
   {  return getValueAsOpaque();
   }

}
