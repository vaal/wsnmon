package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP If-None-Match option (see RFC 7252).
 */
public class IfNoneMatchOption extends CoapOption
{

   /** Creates a new If-None-Match option.
    * @param co CoapOption to be copied */
   public IfNoneMatchOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new If-None-Match option. */
   public IfNoneMatchOption()
   {  super(CoapOptionNumber.IfNoneMatch,EMPTY);
   }

}
