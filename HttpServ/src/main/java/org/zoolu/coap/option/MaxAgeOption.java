package org.zoolu.coap.option;



import org.zoolu.coap.core.CoapOption;



/** CoAP Max-Age option (see RFC 7252).
 */
public class MaxAgeOption extends CoapOption
{

   /** Creates a new MaxAgeOption.
    * @param co CoapOption to be copied */
   public MaxAgeOption(CoapOption co)
   {  super(co);
   }


   /** Creates a new MaxAgeOption.
    * @param max_time the maximum time a response may be cached before it is considered not fresh */
   public MaxAgeOption(int max_time)
   {  super(CoapOptionNumber.MaxAge,max_time);
   }


   /** Gets the maximum time a response may be cached before it is considered not fresh.
    * @return the maximum time */
   public int getMaximumTime()
   {  return (int)getValueAsUnit();
   }

}
