/*
 * Copyright (C) 2008 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.util;



/** Class that collects static methods for dealing with binary materials.
  */
public class ByteUtils
{  
   
   // bytes manipulation:

   /** Compares two arrays of bytes. */
   public static boolean compare(byte[] a, byte[] b)
   {  //if (a.length!=b.length) return false;
      //for (int i=0; i<a.length; i++) if (a[i]!=b[i]) return false;
      //return true;
      return compare(a,0,a.length,b,0,b.length);
   }
   
   /** Compares two arrays of bytes. */
   public static boolean compare(byte[] a_buf, int a_off, int a_len, byte[] b_buff, int b_off, int b_len)
   {  if (a_len!=b_len) return false;
      for (int a_end=a_off+a_len; a_off<a_end; ) if (a_buf[a_off++]!=b_buff[b_off++]) return false;
      return true;
   }
   
   /** Finds an array of bytes (target) within an other array of bytes (source).
     * @return the index of the target array within the source array, or -1 if not found. */
   public static int find(byte[] target, byte[] source_buf, int source_off, int source_len)
   {  return find(target,0,target.length,source_buf,source_off,source_len);
   }

   /** Finds an array of bytes (target) within an other array of bytes (source).
     * @return the index of the target array within the source array, or -1 if not found. */
   public static int find(byte[] target_buf, int target_off, int target_len, byte[] source_buf, int source_off, int source_len)
   {  for (int i=0; i<=(source_len-target_len); i++)
        if (compare(target_buf,target_off,target_len,source_buf,source_off+i,target_len)) return i;
      return -1;
   }

   /** Initalizes a byte array with value <i>value</i>. */
   public static byte[] initBytes(byte[] b, int value)
   {  for (int i=0; i<b.length; i++) b[i]=(byte)value;
      return b;
   }

   /** Rotates the integer (32-bit word) w shifting n bits left.
     * @param w the integer to be rotated
     * @param n the nuber of bits to be shifted to the left */
   /*public static int rotateLeft(int w, int n)
   {  return (w << n) | (w >>> (32-n));
   }*/

   /** Rotates the integer (32-bit word) w shifting n bits right.
     * @param w the integer to be rotated
     * @param n the nuber of bits to be shifted to the right */
   /*public static int rotateRight(int w, int n)
   {  return (w >>> n) | (w << (32-n));
   }*/

   /** Rotates an array of integers (32-bit words), shifting 1 word left.
     * @param w the array of integers to be shifted to the left */
   /*public static int[] rotateLeft(int[] w)
   {  int len=w.length;
      int w1=w[len-1];
      for (int i=len-1; i>1; i--) w[i]=w[i-1];
      w[0]=w1;
      return w;
   }*/

   /** Rotates an array of integers (32-bit words), shifting 1 word right.
     * @param w the array of integers to be shifted to the right */
   /*public static int[] rotateRight(int[] w)
   {  int len=w.length;
      int w0=w[0];
      for (int i=1; i<len; i++) w[i-1]=w[i];
      w[len-1]=w0;
      return w;
   }*/

   /** Rotates an array of bytes, shifting 1 byte left.
     * @param b the array of bytes to be shifted to the left */
   /*public static byte[] rotateLeft(byte[] b)
   {  int len=b.length;
      byte b1=b[len-1];
      for (int i=len-1; i>1; i--) b[i]=b[i-1];
      b[0]=b1;
      return b;
   }*/

   /** Rotates an array of bytes, shifting 1 byte right.
     * @param b the array of bytes to be shifted to the right */
   /*public static byte[] rotateRight(byte[] b)
   {  int len=b.length;
      byte b0=b[0];
      for (int i=1; i<len; i++) b[i-1]=b[i];
      b[len-1]=b0;
      return b;
   }*/

   /** Gets a copy of a given byte array.
     * @param src the original buffer
     * @return the copy array */
   public static byte[] clone(byte[] src)
   {  return getBytes(src,0,src.length);
   }

   /** Gets a <i>len</i>-byte array from a given buffer and offset.
     * @param src the source buffer
     * @param off the offset within the source buffer
     * @param len the length of the returned array
     * @return a <i>len</i>-byte array */
   public static byte[] getBytes(byte[] src, int off, int len)
   {  byte[] bb=new byte[len];
      for (int k=0; k<len; k++) bb[k]=src[off+k];
      return bb;
   }
   
   /** Gets a 2-byte array from a given buffer and offset.
     * @param src the source buffer containing the 2 bytes
     * @param off the offset within the source buffer
     * @return a 2-byte array */
   public static byte[] twoBytes(byte[] src, int off)
   {  return getBytes(src,off,2);
   }
   
   /** Gets a 4-byte array from a given buffer and offset.
     * @param src the source buffer containing the 4 bytes
     * @param off the offset within the source buffer
     * @return a 4-byte array */
   public static byte[] fourBytes(byte[] src, int off)
   {  return getBytes(src,off,4);
   }

   /** Copies all bytes of array <i>src</i> into array <i>dst</i> with offset <i>dst_off</i>. */
   public static int copyBytes(byte[] src, byte[] dst, int dst_off)
   {  //for (int k=0; k<src.length; k++) dst[dst_off+k]=src[k];
      //return src.length;
      return copyBytes(src,0,dst,dst_off,src.length);
   }

   /** Copies the first <i>len</i> bytes of array <i>src</i> into array <i>dst</i> with offset <i>dst_off</i>. */
   public static int copyBytes(byte[] src, byte[] dst, int dst_off, int len)
   {  //for (int k=0; k<len; k++) dst[dst_off+k]=src[k];
      //return len;
      return copyBytes(src,0,dst,dst_off,len);
   }
   
   /** Copies the first <i>len</i> bytes starting from <i>src_off</i> of array <i>src</i> into array <i>dst</i> with offset <i>dst_off</i>. */
   public static int copyBytes(byte[] src, int src_off, byte[] dst, int dst_off, int len)
   {  for (int k=0; k<len; k++) dst[dst_off+k]=src[src_off+k];
      return len;
   }
   
   /** Copies the first 2 bytes of array <i>src</i> into array <i>dst</i> with offset <i>dst_off</i>. */
   public static void copy2Bytes(byte[] src, byte[] dst, int dst_off)
   {  copyBytes(src,dst,dst_off,2);
   }
   
   /** Copies a the first 4 bytes of array <i>src</i> into array <i>dst</i> with offset <i>dst_off</i>. */
   public static void copy4Bytes(byte[] src, byte[] dst, int dst_off)
   {  copyBytes(src,dst,dst_off,4);
   }

   /** Concatenates two byte arrays.
     * @param src1 buffer containing the 1st array
     * @param src2 buffer containing the 2nd array
     * @return a new byte array containing the concatenation of the two arrays. */
   public static byte[] concat(byte[] src1, byte[] src2)
   {  return concat(src1,0,src1.length,src2,0,src2.length);
   }

   /** Concatenates two byte arrays.
     * @param src1 buffer containing the 1st array
     * @param src2 buffer containing the 2nd array
     * @param dst3 buffer for the resulting concatenation array
     * @return the length of the concatenation of the two arrays. */
   public static int concat(byte[] src1, byte[] src2, byte[] dst3)
   {  return concat(src1,0,src1.length,src2,0,src2.length,dst3,0);
   }

   /** Concatenates two byte arrays.
     * @param src1 buffer containing the 1st array
     * @param off1 offset of the 1st array
     * @param len1 length of the 1st array
     * @param src2 buffer containing the 2nd array
     * @param off2 offset of the 2nd array
     * @param len2 length of the 2nd array
     * @return a new byte array containing the concatenation of the two arrays. */
   public static byte[] concat(byte[] src1, int off1, int len1, byte[] src2, int off2, int len2)
   {  byte[] dst3=new byte[len1+len2];
      //copyBytes(src1,off1,dst3,0,len1);
      //copyBytes(src2,off2,dst3,len1,len2);
      concat(src1,off1,len1,src2,off2,len2,dst3,0);
      return dst3;
   }
   
   /** Concatenates two byte arrays.
     * @param src1 buffer containing the 1st array
     * @param off1 offset of the 1st array
     * @param len1 length of the 1st array
     * @param src2 buffer containing the 2nd array
     * @param off2 offset of the 2nd array
     * @param len2 length of the 2nd array
     * @param dst3 buffer for the resulting concatenation array
     * @param off3 offset of the resulting array
     * @return the length of the concatenation of the two arrays. */
   public static int concat(byte[] src1, int off1, int len1, byte[] src2, int off2, int len2, byte[] dst3, int off3)
   {  copyBytes(src1,off1,dst3,off3,len1);
      copyBytes(src2,off2,dst3,off3+len1,len2);
      return len1+len2;
   }



   // bytes to hexadecimal strings, and vice versa:

   /** Gets the unsigned representatin of a byte, returned as a <b>short</b>. The same as getting <i>(short)(b&0xFF)</i>.
     * @param b the byte value
     * @return the unsigned integer value (as <b>short</b>) of the given byte. */
   public static short uByte(byte b)
   {  //return (short)(((short)b+256)&0xFF);
      return (short)(b&0xFF);
   } 

   /** Gets the unsigned representatin of a 32-bit integer, returned as a <b>long</b>. The same as getting <i>(long)n&0x00000000FFFFFFFF</i>.
     * @param n the integer value
     * @return the unsigned integer value (as <b>long</b>) of the given integer. */
   public static long uInt(int n)
   {  return (long)n&0xFFFFFFFFL;
   } 



   // bytes to hexadecimal strings, and vice versa:

   /** Array of hexadecimal digits (from '0' to 'f'). */
   //private static char[] HEX_DIGITS={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

   /** Gets the hexadecimal digit.
     * @param d an integer between 0 and 15 */
   private static char hexDigit(int d)
   {  //if (d<0 || d>15) throw new RuntimeException("Invalid value for a hexadecimal digit: "+d);
      //return (char)(d<10? '0'+d : '7'+d); // uppercase (note: '7'='A'-10)
      return (char)(d<10? '0'+d : 'W'+d); // lowercase (note: 'W'='a'-10)
   }

   /** Gets a hexadecimal representation of an array of bytes.
     * @param data the byte array
     * @return the hexadecimal string */
   public static String asHex(byte[] data)
   {  return asHex(data,0,data.length);
   }  
   
   /** Gets a hexadecimal representation of an array of bytes.
     * @param buf the byte array
     * @param len the number of bytes
     * @return the hexadecimal string */
   /*public static String asHex(byte[] buf, int len)
   {  return asHex(buf,0,len);
   }*/
   
   /** Gets a hexadecimal representation of an array of bytes.
     * @param buf the byte array
     * @param off the offset of the first byte 
     * @param len the number of bytes
     * @return the hexadecimal string */
   public static String asHex(byte[] buf, int off, int len)
   {  char[] str=new char[len*2];
      int index=0;
      int end=off+len;
      while (off<end)
      {  byte b=buf[off++];
         str[index++]=hexDigit((b&0xF0)>>4);
         str[index++]=hexDigit(b&0x0F);
         //str[index++]=HEX_DIGITS[(b&0xF0)>>4]; // fast encoding
         //str[index++]=HEX_DIGITS[b&0x0F]; // fast encoding
      }
      return new String(str);
   }

   /** Gets a colon-separated hexadecimal representation of an array of bytes.
     * The hexadecimal values are formatted in blocks separated by colons ':'.
     * Example, in case <i>num</i>=1 (1-byte blocks) the string will be formatted as "aa:bb:cc:dd:..:ff". 
     * @param data the byte array
     * @param delim the character used as block delimiter
     * @param num the number of bytes within each block
     * @return the hexadecimal string */
   public static String asHexWithColons(byte[] data, int num)
   {  return asHexWithColons(data,0,data.length,num);
   }

   /** Gets a colon-separated hexadecimal representation of an array of bytes.
     * The hexadecimal values are formatted in blocks separated by colons ':'.
     * Example, in case <i>num</i>=1 (1-byte blocks) the string will be formatted as "aa:bb:cc:dd:..:ff". 
     * @param buf the byte array
     * @param off the offset of the first byte 
     * @param len the number of bytes
     * @param delim the character used as block delimiter
     * @param num the number of bytes within each block
     * @return the hexadecimal string */
   public static String asHexWithColons(byte[] buf, int off, int len, int num)
   {  int str_len=len*2+((len+num-1)/num)-1;
      char[] str=new char[str_len];
      int index=0;
      for (int i=0; i<len; i++)
      {  byte b=buf[off+i];
         str[index++]=hexDigit((b&0xF0)>>4);
         str[index++]=hexDigit(b&0x0F);
         //str[index++]=HEX_DIGITS[(b&0xF0)>>4]; // fast encoding
         //str[index++]=HEX_DIGITS[b&0x0F]; // fast encoding
         if (((i+1)%num)==0 && index<str_len) str[index++]=':';
      }
      return new String(str);
   }  

   /** Gets the value of a hexadecimal digit (that is '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', or 'f').
     * @param c the character
     * @return the integer value (between 0 and 15) of the hexadecimal digit */
   private static int hexValue(char c)
   {  return (byte)(c<='9'? c-'0' : (c<='F'? c-'7': c-'W'));
   }

   /** Whether a character is a hexadecimal digit (that is '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', or 'f').
     * @param c the character
     * @return true of it is a valid hexadecimal digit */
   /*private static boolean isHexDigit(char c)
   {  return (c>='0' && c<='9') || (c>='a' && c<='f') || (c>='A' && c<='F');
   }*/

   /** Gets an array of bytes corresponding to a string of hexadecimal values.
     * The string may also include ':' characters that separate blocks of hexadecimal values (e.g. aa:bb:cc:dd:..:ff). 
     * @param str the hexadecimal string
     * @return the array of bytes */
   public static byte[] hexToBytes(String str)
   {  return hexToBytes(str,-1);
   }

   /** Gets an array of bytes corresponding to a string of hexadecimal values.
     * The string may also include ':' characters that separate blocks of hexadecimal values (e.g. aa:bb:cc:dd:..:ff). 
     * @param str the hexadecimal string
     * @param len the length of the array of bytes. If <i>len</i> is set to -1, the entire string is converted.
     * @return the array of bytes */
   public static byte[] hexToBytes(String str, int len)
   {  if (len<0)
      {  // count the number of hex digits (in case there are some ':')
         int str_len=str.length();
         len=0;
         for (int i=0; i<str_len; i++) if (str.charAt(i)!=':') len++;
         len/=2;
      }
      // get the byte array
      byte[] b=new byte[len];
      int index=0;
      char c0,c1;
      for (int i=0; i<len; i++)
      {  //c0=str.charAt(index++);
         while ((c0=str.charAt(index++))==':');
         //c1=str.charAt(index++);
         while ((c1=str.charAt(index++))==':');
         b[i]=(byte)((hexValue(c0)<<4)+hexValue(c1));
      }
      return b;
   }



   // bytes to ascii strings:

   /** Gets an ASCII representation of an array of bytes.
     * Non-ASCII bytes are encoded as '.'.
     * @param data the byte array
     * @return the ASCII string */
   public static String asAscii(byte[] buf)
   {  return asAscii(buf,0,buf.length);
   }  
   
   /** Gets an ASCII representation of an array of bytes.
     * Non-ASCII bytes are encoded as '.'.
     * @param data the byte array
     * @param len the number of bytes
     * @return the ASCII string */
   /*public static String asAscii(byte[] buf, int len)
   {  return asAscii(buf,0,len);
   }*/
   
   /** Gets an ASCII representation of an array of bytes.
     * Non-ASCII bytes are encoded as '.'.
     * @param buf the byte array
     * @param off the offset of the first byte 
     * @param len the number of bytes
     * @return the ASCII string */
   public static String asAscii(byte[] buf, int off, int len)
   {  char[] str=new char[len];
      int index=0;
      int end=off+len;
      while (off<end)
      {  byte b=buf[off++];
         str[index++]=(b>=32 && b<127)? (char)b : '.';
      }
      return new String(str);
   }  



   // bytes to integers, and vice versa:

   /** Transforms a 2-byte array into a 16-bit integer; the 2-byte array is in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param src the 2-byte array representing an integer in big-endian byte order
     * @return the 16-bit integer */
   public static int twoBytesToInt(byte[] src)
   {  return twoBytesToInt(src,0);
   }
   
   /** Transforms a 2-byte array into a 16-bit integer; the 2-byte array is in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param src a buffer containing the 2-byte array representing an integer in big-endian byte order
     * @param off the offset within the buffer
     * @return the 16-bit integer */
   public static int twoBytesToInt(byte[] src, int off)
   {  return (((int)uByte(src[off])<<8)+uByte(src[off+1]));
   }

   /** Transforms a 4-byte array into a 32-bit integer; the 4-byte array is in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param src the 4-byte array representing an integer in big-endian byte order
     * @return the 32-bit integer */
   public static long fourBytesToInt(byte[] src)
   {  return fourBytesToInt(src,0);
   }
   
   /** Transforms a 4-byte array into a 32-bit integer; the 4-byte array is in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param src a buffer containing the 4-byte array representing an integer in big-endian byte order
     * @param off the offset within the buffer
     * @return the 32-bit integer */
   public static long fourBytesToInt(byte[] src, int off)
   {  return ((((((long)uByte(src[off])<<8)+uByte(src[off+1]))<<8)+uByte(src[off+2]))<<8)+uByte(src[off+3]);
   }

   /** Transforms a 8-byte array into a 64-bit integer; the 8-byte array is in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param src the 8-byte array representing an integer in big-endian byte order
     * @return the 64-bit integer */
   public static long eightBytesToInt(byte[] src)
   {  return eightBytesToInt(src,0);
   }
   
   /** Transforms a 8-byte array into a 64-bit integer; the 8-byte array is in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param src a buffer containing the 8-byte array representing an integer in big-endian byte order
     * @param off the offset within the buffer
     * @return the 64-bit integer */
   public static long eightBytesToInt(byte[] src, int off)
   {  return ((((((((((long)uByte(src[off])<<8)+uByte(src[off+1]))<<8)+uByte(src[off+2]))<<8)+uByte(src[off+3])<<8)+uByte(src[off+4])<<8)+uByte(src[off+5])<<8)+uByte(src[off+6])<<8)+uByte(src[off+7]);
   }


   /** Transforms a 16-bit integer into a 2-byte array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 16-bit integer
     * @return a 2-byte array representing the given integer, in big-endian byte order */
   public static byte[] intTo2Bytes(int n)
   {  byte[] b=new byte[2];
      intTo2Bytes(n,b,0);
      return b;
   }

   /** Transforms a 16-bit integer into a 2-byte array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 16-bit integer
     * @param dst a buffer for the 2-byte big-endian representation of the given integer
     * @param off the offset within the buffer */
   public static void intTo2Bytes(int n, byte[] dst, int off)
   {  dst[off]=(byte)(n>>8);
      dst[off+1]=(byte)(n&0xFF);
   }

   /** Transforms a 32-bit integer into a 4-byte array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 32-bit integer
     * @return a 4-byte array representing the given integer, in big-endian byte order */
   public static byte[] intTo4Bytes(long n)
   {  byte[] dst=new byte[4];
      intTo4Bytes(n,dst,0);
      return dst;
   }

   /** Transforms a 32-bit integer into a 4-byte array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 32-bit integer
     * @param dst a buffer for the 4-byte big-endian representation of the given integer
     * @param off the offset within the buffer */
   public static void intTo4Bytes(long n, byte[] dst, int off)
   {  dst[off]=(byte)(n>>24);
      dst[off+1]=(byte)((n>>16)&0xFF);
      dst[off+2]=(byte)((n>>8)&0xFF);
      dst[off+3]=(byte)(n&0xFF);
   }

   /** Transforms a 48-bit integer into a 6-byte array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 48-bit integer
     * @return a 6-byte array representing the given integer, in big-endian byte order */
   public static byte[] intTo6Bytes(long n)
   {  byte[] dst=new byte[6];
      intTo6Bytes(n,dst,0);
      return dst;
   }

   /** Transforms a 48-bit integer into a 6-byte array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 48-bit integer
     * @param dst a buffer for the 6-byte big-endian representation of the given integer
     * @param off the offset within the buffer */
   public static void intTo6Bytes(long n, byte[] dst, int off)
   {  dst[off]=(byte)(n>>40);
      dst[off+1]=(byte)((n>>32)&0xFF);
      dst[off+2]=(byte)((n>>24)&0xFF);
      dst[off+3]=(byte)((n>>16)&0xFF);
      dst[off+4]=(byte)((n>>8)&0xFF);
      dst[off+5]=(byte)(n&0xFF);
   }

   /** Transforms a 64-bit signed integer (as long) into a 8-bytes array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 64-bit integer
     * @return a 8-byte array representing the given integer, in big-endian byte order */
   public static byte[] intTo8Bytes(long n)
   {  byte[] dst=new byte[8];
      intTo8Bytes(n,dst,0);
      return dst;
   }

   /** Transforms a 64-bit signed integer (as long) into a 8-bytes array in big-endian byte order (the big end, most significant byte, is stored first, at the lowest storage address).
     * @param n the 64-bit integer
     * @param dst a buffer for the 8-byte big-endian representation of the given integer
     * @param off the offset within the buffer */
   public static void intTo8Bytes(long n, byte[] dst, int off)
   {  dst[off]=(byte)((n>>56)&0xFF);
      dst[off+1]=(byte)((n>>48)&0xFF);
      dst[off+2]=(byte)((n>>40)&0xFF);
      dst[off+3]=(byte)((n>>32)&0xFF);
      dst[off+4]=(byte)((n>>24)&0xFF);
      dst[off+5]=(byte)((n>>16)&0xFF);
      dst[off+6]=(byte)((n>>8)&0xFF);
      dst[off+7]=(byte)(n&0xFF);
   }



   /** Transforms a 4-byte array into a 32-bit integer; the 4-byte array is in little-endian byte order (the little end, least significant byte, is stored first, at the lowest storage address).
     * @param src the 4-byte array representing an integer in little-endian byte order
     * @return the 32-bit integer */
   public static long fourBytesToIntLittleEndian(byte[] src)
   {  //return ((((((long)uByte(src[3])<<8)+uByte(src[2]))<<8)+uByte(src[1]))<<8)+uByte(src[0]);
      return fourBytesToIntLittleEndian(src,0);
   }
   
   /** Transforms a 4-byte array into a 32-bit integer; the 4-byte array is in little-endian byte order (the little end, least significant byte, is stored first, at the lowest storage address).
     * @param src a buffer containing the 4-byte array representing an integer in little-endian byte order
     * @param off the offset within the buffer
     * @return the 32-bit integer */
   public static long fourBytesToIntLittleEndian(byte[] src, int off)
   {  return ((((((long)uByte(src[off+3])<<8)+uByte(src[off+2]))<<8)+uByte(src[off+1]))<<8)+uByte(src[off]);
   }



   /** Transforms a 32-bit integer into a 4-byte array in little-endian byte order (the little end, least significant byte, is stored first, at the lowest storage address).
     * @param n the 32-bit integer
     * @return a 4-byte array representing the given integer, in little-endian byte order */
   public static byte[] intTo4BytesLittleEndian(long n)
   {  byte[] dst=new byte[4];
      //buf[3]=(byte)(n>>24);
      //buf[2]=(byte)((n>>16)&0xFF);
      //buf[1]=(byte)((n>>8)&0xFF);
      //buf[0]=(byte)(n&0xFF);
      intTo4BytesLittleEndian(n,dst,0);
      return dst;
   }

   /** Transforms a 32-bit integer into a 4-byte array copied into a given buffer; the 4-bytes array is in little-endian byte order (the little end, least significant byte, is stored first, at the lowest storage address).
     * @param n the 32-bit integer
     * @param dst a buffer for the 4-byte little-endian representation of the given integer
     * @param off the offset within the buffer */
   public static void intTo4BytesLittleEndian(long n, byte[] dst, int off)
   {  dst[off+3]=(byte)(n>>24);
      dst[off+2]=(byte)((n>>16)&0xFF);
      dst[off+1]=(byte)((n>>8)&0xFF);
      dst[off]=(byte)(n&0xFF);
   }



   /** Modifies the given array inverting the byte order.
     * It transforms little-endian to big-endian n-bytes array (and vice versa).
     * @param buf the buffer containing the array
     * @return the same buffer with the inverted order */
   public static byte[] invertByteOrder(byte[] buf)
   {  //byte[] b1=new byte[buf.length];
      //for (int i=0,j=buf.length-1; i<b1.length; i++,j--) b1[i]=buf[j];
      //return b1;
      for (int i=0,j=buf.length-1; i<buf.length/2; i++,j--)
      {  byte b_i=buf[i];
         buf[i]=buf[j];
         buf[j]=b_i;
      }
      return buf;
   }

   /** Adds 1 to the number represented by the given array of bytes, module 2^n, where n is the length (in bits) of the array.
     * @param buf the buffer containing the array
     * @return the same buffer, incremented in value */
   public static byte[] inc(byte[] buf)
   {  for (int i=buf.length-1; i>=0; i--)
      {  if ((buf[i]=(byte)(((buf[i]&0xFF)+1)&0xFF))!=0) break;
      }
      return buf;
   }

   /** Gets a 16-bit integer from 2 bytes.
     * @param b_high high byte
     * @param b_low low byte
     * @return the 16-bit integer value */
   public static int twoBytesToInt(byte b_high, byte b_low)
   {  return ((((int)b_high)&0xFF)<<8) | (((int)b_low)&0xFF);
   }

   /** Gets the lower byte of a 16bit integer.
     * @param i the int value
     * @return the value of the lower byte */
   public static byte byteLow(int i)
   {  return (byte)(i&0xFF);
   }

   /** Gets the higher byte of a 16bit integer.
     * @param i the int value
     * @return the value of the higher byte */
   public static byte byteHigh(int i)
   {  return (byte)((i&0xFF00)>>8);
   }
 
   /** Gets a 32-bit integer (as long) from 4 bytes.
     * @param b3 the 4rd byte
     * @param b2 the 3rd byte
     * @param b1 the 2nd byte
     * @param b0 the 1st byte
     * @return the 32-bit integer value */
   public static long fourBytesToLong(byte b3, byte b2, byte b1, byte b0)
   {  return ((((long)b3)&0xFF)<<24) | ((((long)b2)&0xFF)<<16) | ((((long)b1)&0xFF)<<8) | (((long)b0)&0xFF);
   }

   /** Gets the 1st byte of a 32bit integer (i.e. the lower byte).
     * @param i the integer value
     * @return the value of the 1st byte */
   public static byte byte0(long i)
   {  return (byte)(i&0xFF);
   }

   /** Gets the 2nd byte of a 32bit integer.
     * @param i the integer value
     * @return the value of the 2nd byte */
   public static byte byte1(long i)
   {  return (byte)((i&0xFF00)>>8);
   }

   /** Gets the 3rd byte of a 32bit integer.
     * @param i the integer value
     * @return the value of the 3rd byte */
   public static byte byte2(long i)
   {  return (byte)((i&0xFF0000)>>16);
   }

   /** Gets the 4rd byte of a 32bit integer (i.e. the higher byte).
     * @param i the integer value
     * @return the value of the 4rd byte (the last) */
   public static byte byte3(long i)
   {  return (byte)((i&0xFF000000)>>24);
   }

}
