/*
 * Copyright (C) 2014 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.util;



import java.io.*;
import java.util.Date;



/** A Logger writes log messages onto a logfile, the standard output, or any <i>java.io.Writer</i>
  * or <i>java.io.PrintStream</i>.
  * <p>
  * When creating a Logger you can also specifiy a <i>verbose_level</i> and a <i>maximum_size</i> for the log.
  * <p/>
  * The attribute <i>verbose_level</i> is used do manage different levels of verboseness.
  * When adding a log message through the methods {@link Logger#append(int level, String message) append(level,message)}
  * or {@link Logger#append(int level, Class source_class, String message) append(level,source_class,message)}
  * you can indicate a <i>log_level</i> for the message; only log messages with a <i>log_level</i>
  * less or equal to the <i>verbose_level</i> are actually recorded.
  * Log messages added through the method {@link Logger#append(String message) append(message)} corresponds to
  * a log level {@link Logger#LEVEL_HIGH HIGH}.
  * <br/>
  * Verbose level {@link Logger#LEVEL_OFF} (equal to 0) indicates no log.
  * <p/>
  * The attribute <i>maximum_size</i> is used to limit the size the log.
  * When the log size reaches the <i>maximum_size</i> value, no more log messages are recoreded.
  */
public class Logger
{
   /** Level for hight priority logs. */
   public static final int LEVEL_HIGH=1;

   /** Level for medium priority logs. */
   public static final int LEVEL_MEDIUM=2;

   /** Level for low priority logs. */
   public static final int LEVEL_LOW=3;  

   /** Level for very low priority logs. */
   public static final int LEVEL_DEBUG=4; 

   /** Level for no logs. */
   public static final int LEVEL_OFF=0; 

   /** Default maximum log file size (1MB) */
   public static long DEFAULT_MAX_SIZE=1024*1024; // 1MB

   /** Default Logger */
   protected static Logger DEFAULT_LOGGER=null;




   /** The log writer */
   protected Writer out;

   /** The <i>verbose_level</i>.
     * Only messages with a level less or equal this <i>verbose_level</i> are logged.
     * If <i>verbose_level</i> is less or equal to 0, messages are not logged. */
   protected int verbose_level;
   
   /** The maximum size of the log stream/file [bytes]
     * Value 0 (or negative) indicates no maximum size */
   protected long max_size;
     
   /** Whether writing a timestamp header */
   boolean timestamp=true;

   /** The char counter of the already logged data */
   long counter;




   /** Creates a new Logger.
     * @param out the Writer where log messages are written to */
   public Logger(Writer out)
   {  reset(out,1,0);
   }


   /** Creates a new Logger.
     * @param out the Writer where log messages are written to 
     * @param verbose_level the verbose level */
   public Logger(Writer out, int verbose_level)
   {  reset(out,verbose_level,0);
   }


   /** Creates a new Logger.
     * @param out the OutputStream where log messages are written to */
   public Logger(OutputStream out)
   {  reset(new OutputStreamWriter(out),1,0);
   }


   /** Creates a new Logger.
     * @param out the OutputStream where log messages are written to
     * @param verbose_level the verbose level */
   public Logger(OutputStream out, int verbose_level)
   {  reset(new OutputStreamWriter(out),verbose_level,0);
   }


   /** Creates a new the Logger.
     * @param file_name the file where log messages are written to */
   public Logger(String file_name)
   {  init(file_name,1,DEFAULT_MAX_SIZE,false);
   }


   /** Creates a new the Logger.
     * @param file_name the file where log messages are written to
     * @param verbose_level the verbose level */
   public Logger(String file_name, int verbose_level)
   {  init(file_name,verbose_level,DEFAULT_MAX_SIZE,false);
   }


   /** Creates a new the Logger.
     * @param file_name the file where log messages are written to
     * @param verbose_level the verbose level
     * @param max_size the maximum size for the log file, that is the maximum number of characters that can be wirtten */
   public Logger(String file_name, int verbose_level, long max_size)
   {  init(file_name,verbose_level,max_size,false);
   }


   /** Creates a new the Logger.
     * @param file_name the file where log messages are written to
     * @param verbose_level the verbose level
     * @param max_size the maximum size for the log file, that is the maximum number of characters that can be wirtten
     * @param append the file is opened in 'append' mode, that is the new messages are appended to the previously saved file (the file is not rewritten) */
   public Logger(String file_name, int verbose_level, long max_size, boolean append)
   {  init(file_name,verbose_level,max_size,append);
   }


   /** Initializes the Logger.
     * @param file_name the file where log messages are written to
     * @param verbose_level the verbose level
     * @param max_size the maximum size for the log file, that is the maximum number of characters that can be wirtten
     * @param append the file is opened in 'append' mode, that is the new messages are appended to the previously saved file (the file is not rewritten) */
   private void init(String file_name, int verbose_level, long max_size, boolean append)
   {  if (verbose_level>0)
      {  try
         {  Writer out=new OutputStreamWriter(new FileOutputStream(file_name,append));
            reset(out,verbose_level,max_size);
         }
         catch (IOException e)
         {  e.printStackTrace();
         }
      }
      else reset(null,0,0);
   }


   /** Resets the Logger.
     * @param out the Writer where log messages are written to
     * @param verbose_level the verbose level
     * @param max_size the maximum size for the log, that is the maximum number of characters that can be written */
   protected void reset(Writer out, int verbose_level, long max_size) 
   {  this.out=out;
      this.verbose_level=verbose_level;
      this.max_size=max_size;
      counter=0;
   }


   /** Sets the default logger.
     * @param default_logger the default logger */
   public static void setDefaultLogger(Logger default_logger) 
   {  DEFAULT_LOGGER=default_logger;
   }


   /** Gets the deafult logger.
     * @return the default logger */
   public static Logger getDefaultLogger() 
   {  return DEFAULT_LOGGER;
   }


   /** Sets the verbose level.
     * @param verbose_level the verbose level */
   public void setVerboseLevel(int verbose_level) 
   {  this.verbose_level=verbose_level;
   }


   /** Gets the current verbose level.
     * @return the verbose level */
   public int getVerboseLevel() 
   {  return verbose_level;
   }


   /** Enables or disables writing a timestamp header.
     * @param timestamp true for enabling the use of timestamps */
   public void setTimestamp(boolean timestamp) 
   {  this.timestamp=timestamp;
   }


   /** Close the logger. */
   public void close()
   {  if (out!=null) try {  out.close();  } catch (IOException e) {  e.printStackTrace();  }
      out=null;
   }


   /** Adds a log message.
     * @param message the message to be logged */
   public void append(String message)
   {   append(1,null,message);
   }


   /** Adds a log message.
     * @param level the log level of this message; only messages with log level less than or equal to the <i>verbose_level</i> of the logger are actually recorded
     * @param message the message to be logged */
   public void append(int level, String message)
   {   append(level,null,message);
   }


   /** Adds a log message, if <i>level</i> is less than or equal to the <i>verbose_level</i>.
     * @param level the log level of this message; only messages with log level less than or equal to the <i>verbose_level</i> of the logger are actually recorded
     * @param source_class the origin of this log message
     * @param message the message to be logged */
   public synchronized void append(int level, Class source_class, String message)
   {  if (out!=null && level>0 && level<=verbose_level && (max_size<=0 || counter<max_size))
      {  StringBuffer sb=new StringBuffer();
         if (timestamp) sb.append(DateFormat.formatHHmmssSSS(new Date(System.currentTimeMillis()))).append(": ");
         if (source_class!=null) sb.append(source_class.getName().substring(source_class.getPackage().getName().length()+1)).append(": ");
         message=sb.append(message).append("\r\n").toString();
         try
         {  out.write(message);
            out.flush();
            counter+=message.length();
            if (max_size>0 && counter>=max_size)
            {  out.write("\r\n----MAXIMUM LOG SIZE----\r\nSuccessive logs are lost.");
               out.flush();
            }
         }
         catch (Exception e) {}
      }
   }

}
