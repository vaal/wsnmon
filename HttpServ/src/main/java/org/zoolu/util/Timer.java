/*
 * Copyright (C) 2006 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */

package org.zoolu.util;



//import java.util.HashSet;
//import java.util.Iterator;



/** A Timer is a simple object that fires an onTimeout() method to its TimerListener when the time expires.
  * A Timer have to be explicitely started, and can be halted before it expires.
  */
public class Timer /*extends org.zoolu.util.MonitoredObject*/ //implements InnerTimerListener
{
   /** Whether using single thread for all timer instances. */
   public static boolean SINGLE_THREAD=true; 

   //HashSet listener_list=null;

   /** Timer listener */
   protected TimerListener listener;

   /** Timeout value */
   protected long time;
     
   /** Whether this timer is active (running) */
   protected boolean active=false;

   /** Start time */
   long start_time=0;
     


   /** Creates a new Timer.
     * <p/>
     * The Timer is not automatically started. You need to call the {@link #start()} method.
     * @param time the timer expiration time in milliseconds
     * @param listener timer listener */
   public Timer(long time, TimerListener listener)
   {  //listener_list=new HashSet();
      //if (listener!=null) addTimerListener(listener);
      this.listener=listener;
      this.time=time;
   }  

   /** Gets the initial time.
     * @return the initial time in milliseconds */
   public long getTime()
   {  return time;
   }
   
   /** Gets the remaining time.
     * @return the remaining time in milliseconds */
   public long getExpirationTime()
   {  if (active)
      {  long expire=start_time+time-System.currentTimeMillis();
         return (expire>0)? expire : 0;
      }
      else return time;
   }
   
   /** Adds a new listener (TimerListener). */
   /*public void addTimerListener(TimerListener listener)
   {  listener_list.add(listener);
   }*/

   /** Removes the specific listener (TimerListener). */
   /*public void removeTimerListener(TimerListener listener)
   {  listener_list.remove(listener);
   }*/
   
   /** Starts the timer. */
   public void start()
   {  start(SINGLE_THREAD);
   }

   /** Starts the timer.
     * @param single_thread whether running the timer in a single thread in common with other timer instances. */
   public synchronized void start(boolean single_thread)
   {  if (time<0) return;
      // else
      start_time=System.currentTimeMillis();
      active=true;
      if (time>0)
      {  InnerTimerListener t_listener=new InnerTimerListener()
         {  public void onInnerTimeout() {  processInnerTimeout();  }   
         };
         if (single_thread) new InnerTimerST(time,t_listener);
         else new InnerTimerMT(time,t_listener);
      }
      else
      {  // fire now!
         processInnerTimeout();  
      }
   }   


   /** Whether the timer is running. */
   public boolean isRunning()
   {  return active;
   }   


   /** Stops the Timer. The method {@link TimerListener#onTimeout(Timer)} of the timer listener will not be fired. */
   public synchronized void halt()
   {  active=false;
      //listener_list=null;
      listener=null;
   }

   /** When the InnerTimer expires. */
   private synchronized void processInnerTimeout()
   {  //if (active && !listener_list.isEmpty())
      //{  for (Iterator i=listener_list.iterator(); i.hasNext(); )
      //   {  ((TimerListener)i.next()).onTimeout(this);
      //   }
      //}   
      if (active && listener!=null) listener.onTimeout(this);  
      active=false;
      //listener_list=null;
      listener=null;
   }
   



   /** Listen for an InnerTimer.
     */
   interface InnerTimerListener
   {
      /** When the Timeout fires */
      public void onInnerTimeout();   
   }



   /** Whether running as separate daemon */
   static final boolean IS_DAEMON=true;

   /** Maximum number of attempts to schedule an instance of InnerTimerST */
   static final int MAX_ATTEPTS=2;

   /** Timer used to schedule all InnerTimerST instances */
   static java.util.Timer single_timer=new java.util.Timer(IS_DAEMON);


   /** Class InnerTimerST implements a single-thread timer.
     * InnerTimerST uses the java.util.Timer in order to share the same thread
     * with all other InnerTimerST instances (that are simply viewed as java.util.Timer's tasks).
     * <p/>
     * If an error occurs while scheduling the InnerTimerST,
     * a new Timer is istatiated and the InnerTimerST is re-scheduled.
     */
   class InnerTimerST extends java.util.TimerTask
   {
      /** Timer listener */
      InnerTimerListener listener;
      
      /** Creates a new InnerTimerST. */
      public InnerTimerST(long timeout, InnerTimerListener listener)
      {  this.listener=listener;
         int attempts=0;
         boolean success=false;
         while (!success && attempts<MAX_ATTEPTS)
         {  try 
            {  single_timer.schedule(this,timeout);
               success=true;
            }
            catch (IllegalStateException e)
            {  attempts++;
               single_timer=new java.util.Timer(IS_DAEMON);
            }
         }
      }  
   
      /** From TimerTask. The action to be performed by this timer task. */
      public void run()
      {  if (listener!=null)
         {  listener.onInnerTimeout();
            listener=null;
         }
      }   
   }
   
   
   
   /** Class InnerTimerMT implements a separated-thread timer */
   class InnerTimerMT extends Thread
   {
      /** Timeout value */
      long timeout;
   
      /** Timer listener */
      InnerTimerListener listener;
      
      /** Creates a new InnerTimerMT. */
      public InnerTimerMT(long timeout, InnerTimerListener listener)
      {  this.timeout=timeout;
         this.listener=listener;
         start(); 
      }  
   
      /** From Thread. Runs in a separated thread. */
      public void run()
      {  if (listener!=null)
         {  try
            {  Thread.sleep(timeout);
               listener.onInnerTimeout();
            }
            catch (Exception e) { e.printStackTrace(); }
            listener=null;
         }
      }   
   }

}
