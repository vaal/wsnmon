package model;

import java.util.Date;

public class Value {
	
	Double temperature;
	Double humidity;
	Double light;
	
	public Value(Double temperature, Double humidity, Double light) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.light = light;
	}

	/**
	 * @return temperature
	 */
	public Double getTemperature() {
		return temperature;
	}

	/**
	 * @param temperature temperature da impostare
	 */
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	/**
	 * @return humidity
	 */
	public Double getHumidity() {
		return humidity;
	}

	/**
	 * @param humidity humidity da impostare
	 */
	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	/**
	 * @return light
	 */
	public Double getLight() {
		return light;
	}

	/**
	 * @param light light da impostare
	 */
	public void setLight(Double light) {
		this.light = light;
	}
	
	public String toCSV() {
		
		return new Date().getTime()+"," +this.temperature+","+this.humidity+","+this.light;
	}
	

}
