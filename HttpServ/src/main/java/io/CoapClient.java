package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.zoolu.coap.core.CoapMessage;
import org.zoolu.coap.core.CoapProvider;
import org.zoolu.coap.core.CoapTransactionClient;
import org.zoolu.coap.core.CoapTransactionClientListener;
import org.zoolu.coap.message.CoapMessageFactory;
import org.zoolu.coap.message.CoapMethod;
import org.zoolu.coap.message.CoapRequest;
import org.zoolu.net.SocketAddress;
import org.zoolu.util.ByteUtils;


public class CoapClient implements CoapTransactionClientListener {

	private CoapProvider coapProvider;
	private Map<String,String> responseMap = new HashMap<String,String>();


	
	public CoapClient(int port) throws SocketException {
		this.coapProvider = new CoapProvider(port);
	}


	public ArrayList<String> discovery(String borderRouterIP) throws Exception {
		URL url = new URL(borderRouterIP);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append((inputLine));
		}
		in.close();

		String parsed = "";
		ArrayList<String> addresses = new ArrayList<String>();
		parsed = response.substring(response.indexOf("Routes<pre>") + "Routes<pre>".length(), response.indexOf("</pre></body>"));
		for(String split : parsed.split("fefe:3::1")) {
			if(split.isEmpty()) {
				continue;
			}
			addresses.add("fefe:3::1".concat(split.split("/")[0]));
			System.out.println("fefe:3::1".concat(split.split("/")[0]));
		}

		return addresses;
	}

	public void GET(String resource) throws URISyntaxException {
		URI resource_uri = new URI(resource);
		SocketAddress server_soaddr = new SocketAddress(resource_uri.getHost(),resource_uri.getPort());
		CoapRequest req = CoapMessageFactory.createCONRequest(CoapMethod.GET,resource_uri);
		
		new CoapTransactionClient(coapProvider,server_soaddr,this).request(req);
	}


	@Override
	public void onTransactionResponse(CoapTransactionClient tc, CoapMessage resp) {
		//System.out.println(CoapResponseCode.getDescription(resp.getCode()));
		byte[] payload=resp.getPayload();
		String value =ByteUtils.asAscii(resp.getPayload());
		responseMap.put((resp.getRemoteSoAddress().getAddress().toString()), value);
		if (payload!=null)
			System.out.println(new String(payload));

		coapProvider.halt();
	}


	@Override
	public void onTransactionFailure(CoapTransactionClient tc) {
		System.out.println("Failure..");
		coapProvider.halt();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CoapClient client;
		
		try {
			File output = new File("out.txt");
			client = new CoapClient(CoapProvider.ANY_PORT);
			ArrayList<String> ipDevices = client.discovery("http://[fefe:3:0:1:212:7400:116d:f0cc]");
			//ipDevices.add("fefe:3::1:c30c:0:0:5c");
			for (int i=0; i< ipDevices.size();i++) {
				//System.out.println(ipDevices.get(i));
				//client.GET("coap://[aaaa"+ipDevices.get(i).substring(4)+"]/sensors/light");
				client.GET("coap://["+ipDevices.get(i)+"]/sensors/");
//				client.GET("coap://["+ipDevices.get(i)+"]/sensors/humidity");
//				client.GET("coap://["+ipDevices.get(i)+"]/sensors/light");
			}

			FileWriter fileWritter = new FileWriter(output.getName(),true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			
			for (int i=0; i<100000000; i++) {
				
			}
			
			for (String key : client.responseMap.keySet()) {
				System.out.println("value:"+client.responseMap.get(key));
				bufferWritter.write(new Date().getTime()+","+key+","+client.responseMap.get(key)+"\n");
				bufferWritter.close();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
